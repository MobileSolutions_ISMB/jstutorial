class Component {
    render() {
        if (!this.template) {
            throw new Error('A component must have a template!');
        }
        
        // Very naive, no more than a proof of concept!
        return this.template.replace(
                /\{[a-z0-9\-_]+}/ig, 
                v => {
                    let value = this[v.substring(1, v.length - 1)];
                    if (!value)
                        return;
                    
                    if (value.constructor === Array) {
                        return value.join(' ');
                    }
                         
                    return value;
                }
        );
    }
    
    toString() {
        return this.render();
    }
}

export default Component;