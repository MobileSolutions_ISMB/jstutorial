import Component from './Component';
import moment from 'moment';

class Message extends Component {
    constructor({author, text, date}) {
        super();
        
        let formattedDate = moment(new Date(date));
        
        this.author = author;
        this.date = formattedDate.format('MMMM Do YYYY, hh:mm:ss');
        this.text = text;
        
        this.template = require('html!templates/message.html');
    }
}

export default Message;