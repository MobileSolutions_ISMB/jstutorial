import jQuery from 'jquery';
import http from 'axios';

import Component from './Component';
import Message from './Message';

class Application extends Component {
    constructor() {
        super();

        this.template = require('html!templates/main.html');
        console.log(this.template);
    }

    start(selector) {
        let container = document.querySelector(selector);
        if (!container) {
            throw new Error(`Container ${selector} not found!`);
        }

        this.container = container;
        this.messages = [];

        this.fetchMessages();
    }

    refresh() {
        jQuery(this.container).html(this.render());

        jQuery('#new-message-form', this.container)
            .on('submit', (event) => {
                event.preventDefault();

                let author = jQuery('[name=author]', '#new-message-form').val();
                let text = jQuery('[name=text]', '#new-message-form').val();

                this.postMessage(author, text);

                return false;
            });
    }

    postMessage(author, text) {
        console.log('Posting new message to the server...');

        if (!author || !text)
            return;

        http.post('http://localhost:3000/posts', {author, text})
            .then(
                response => {
                    let result = response.data;

                    this.messages = [new Message(result.data), ...this.messages];
                    this.refresh();
                },
                error => {
                    console.log(error);
                }
            )
    }

    fetchMessages() {
        console.log('Downloading messages from server...');

        http.get('http://localhost:3000/posts')
            .then(
                response => {
                    this.messages = response.data.data.map(d => new Message(d)).reverse() || [];

                    this.refresh();
                },

                error => {
                    console.log(error);
                }
            )
    }
}

export default Application;
