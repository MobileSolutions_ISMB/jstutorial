global.jQuery = require('jquery');
require('bootstrap/dist/js/bootstrap.js');
require('bootstrap/dist/css/bootstrap.css');

import Application from 'modules/Application';

let app = new Application();
app.start('#main');
