# 2. ES6

## 2.1. Introduction

Just like most other programming and scripting languages, JavaScript has evolved over time. Currently, the most supported version of JavaScript is dubbed ES5 and was standardized back in 2009. In 2015 a newer, improved version of the language has been formalized and approved as a standard, and is currently being implemented in most modern browser. This new version, called ES6 or ES2015, introduces numerous new features while being completely backwards compatible.

The JavaScript landscape is rapidly changing and it's no surprise that the new standard is already being heavily employed even without full browser support. How does one go about running ES6 in a browser that doesn't support it yet? The answer is: _transpilers_. Transpilers are tools that, simply put, take ES6 code and convert it to regular ES5 code - possibly optimizing it in the process. The concept of transpiling isn't new: these very same tools have been used for years to translate code written in 'dialects', such as CoffeeScript or TypeScript, to regular JavaScript code.

To support ES6 in our projects we are going to use one specific transpiler, the one that has been gaining the most traction and is currently the most widely adopted: _Babel_ ([official website](http://babeljs.io/)). Babel can be used as a stand-alone tool but also seamlessly integrates with Webpack as a loader, changing the way Webpack interacts with JavaScript files and making it possible to output regular, ES5-compliant code from ES6 sources. With the use of sourcemap, you'll even be able to debug your project using the original ES6 code!

## 2.2. Webpack Configuration

In order to support ES6, we first and foremost need to install a couple of development dependencies:

```
npm install babel-loader babel-core babel-preset-es2015 --save-dev
```

This will install both the core of the Babel transpiler, the Webpack loader and the ES2015 definitions locally in your project's `node_modules` folder.

> We are going to use the newest version of Babel, which is version 6. This version has some breaking changes from version 5 and is more picky about how modules are imported. Since Babel 5 is widely adopted, you should be careful when querying about your issues, and make sure that you are reading about Babel 6 and not Babel 5.

After installing the dependecies, all there's left to do is to tell Webpack to use the `webpack-loader` to parse JavaScript files (regardless of the standard they're written in). This is achieved by adding an entry under the `loader` configuration key:

```javascript
module.exports = {
    ...
    module: {
        loaders: [
            ...
			{
				test: /\.js$/,
				exclude: /(node_modules)/,
				loader: 'babel',
				query: {
					presets: ['es2015']
				}
			}
            ...       
        ]  
    },
    ...
}
```

> Note: the query for a loader can either be specified in a URL like format (?query=something) after its name in the `loader` key or as a standalone `query` object, containing a key for each parameter.

If you modify your `webpack.config.js` and add the loader as specified in the code above, then run `webpack` again, you'll notice no change: your project will still be compiled and bundled within a single `app.js` file within the `dist` folder. This means that everything is working!

Now that we have configured our project to support ES6, we can begin writing some code. We're going to write a simple _Notice Board_ application, where users will be able to freely post messages and see messages that have been posted so far either by them or by other users.

This application is going to have a small NodeJS server and a very, very simple frontend application which will be written with the sole purpose of showing off ES6 features and is **not** to be considered a good example on how to structure a web application - it doesn't actually have a real structure and solves the problem in a very straight forward way which is not suitable for real-life applications.  

## 2.3. The server application

The server application written for this example uses the `express` Node package to spawn a webserver listening on the port 3000. It exposes a minimal API that allows anyone to retrieve and post messages. The API is structured as follows:

- `GET http://<server_address>:3000/posts` retrieves all the messages stored on the server. The application's data is not persistent, so all data will be lost once the server is killed;

- `POST http://<server_address>:3000/posts` receives a JSON payload containing an object with two keys: `author` and `text`. If both keys contain something, a message will be added and the response will contain such message in the same format it was sent to the server, with an additional `date` field containing an UTC String representing the date the message was added to the server.

Both endpoints expect and send back HTTP payloades encoded as `application/json`.

> While _how_ the server does what it does is mostly irrelevant, and is done in a very naïve way, please note that it is packaged using Webpack. This is because I've written it using some ES6 notation, and one way to have NodeJS play nice with ES6 is to transpile the code to ES5. This can also be achieved by using the `babel-node` command, but I wouldn't recommend it for production code.

In order to run the server application, move to the `server` folder and, after installing its dependencies (`npm install`), type `npm start`. The server will run until you kill it by pressing `CTRL+C`.  

## 2.4. Classes and Modules

Let's shift our focus to the client application, the one that will run in our browser. One of the greatest advantages of ES6 over ES5 is the native ability to declare and import modules when needed, and modules are often (but not always) defined as _classes_.

> It's important to note that JavaScript does _not_ use a class model, like most OO languages do. Instead, it uses [_prototypal inheritance_](https://en.wikipedia.org/wiki/Prototype-based_programming) to compose "objects". A "prototype" is nothing more than an object that defines a specific behavior. Inheriting a prototype means that a derived object uses the "parent" object as its prototype, and defines more methods on itself to extend its own functionality.

> Why is this important? Because even though ES6 introduces _classes_ to define objects, this is nothing more than syntactic sugar and the underlying paradigm is still that of prototypal inheritance. *Always* keep this in mind.

We'll start a new project from a folder containing only a `webpack.config.js` (configured to support babel) file and a `package.json` file (with `babel` among its development dependencies, as specified in 2.2). Let's create a `modules` folder within our `src` folder, so that our folder structure looks like this:

```
    + dist/
    + src/
        + modules/
        index.html
        index.js
    + node_modules/
    package.json
    webpack.config.js
```

We can now create an `Application.js` file within the `modules` directory in order to define our `Application` module:

```javascript
import jQuery from 'jquery';

class Application {
    constructor() {

    }

    start(selector) {
        let container = document.querySelector(selector);
        if (!container) {
            throw new Error(`Container ${selector} not found!`);
        }   

        this.container = container;

        this.render();
    }

    render() {
        jQuery(this.container).html("<h1>Hello!</h1>");
    }
}

export default Application;
```

There is quite a bit going on here, so let's go through everything:

- We _imported_ an [_external dependency_](http://babeljs.io/docs/learn-es2015/#modules), `jQuery`, using ES6 `import` notation. A module can _export_ one or more objects, as we'll see later.

- We defined a [_class_](http://babeljs.io/docs/learn-es2015/#classes) called `Application`, containing a `constructor` and two instance methods: `start` and `render`.

> Take notice of how, within the `start` method, the `container` variable is declared using the `let` keyword: ES6 introduces two new [variable declaration operators](http://babeljs.io/docs/learn-es2015/#let-const): `let` and `const`. The `let` keyword works like the old `var` keyword, except that variables declared with `let` are valid only within the scope they are declared in and the immediate children scope; the `const` keyword, as its name suggests, is used to declare constant variables that are not going to change over time. Both this keywords prevent [variable hoisting](http://www.w3schools.com/js/js_hoisting.asp).

- We _exported_ this class as the _default_ export for this module. A module can export multiple objects, and can have a `default` export that, if specified, will be automatically selected when importing from the module.

> A module `myModule` can export multiple items:
```
export function sum(a, b) { return a + b; }
export function divide(a, b) { return a / b; }
export default function greet(name) { return `Hello ${name}!`; }
```
These items can be imported individually:
```
import { sum, divide } from 'myModule'
import greet from 'myModule'
```

- We used ES6 [_string interpolation_](http://babeljs.io/docs/learn-es2015/#template-strings) facilities to compose a string. A string enclosed within \`s (grave accents) will interpolate any variable that is specified within it when included using the `${}` notation.  

While this can seem like it's not much, using these ES6 facilities allows for a more modular, clean and maintainable way of writing applications.

## 2.5. Writing functions, the ES6 way

Let's continue exploring ES6. We're going to create another class within the `modules` directory called `Component`. This class will represent a _component_, which is something that gets drawn on the DOM and may or may not have some logic attached to it. We're going to have two components in this application: the _Main_ component, containing the whole app, and the _Message_ component, containing a single message; we could (and should) have many more, but let's keep things minimal.

```javascript
class Component {
    render() {
        if (!this.template) {
            throw new Error('A component must have a template!');
        }

        return this.template.replace(
                /\{[a-z0-9\-_]+}/ig,
                v => {
                    let value = this[v.substring(1, v.length - 1)];
                    if (!value)
                        return;

                    if (value.constructor === Array) {
                        return value.join(' ');
                    }

                    return value;
                }
        );
    }

    toString() {
        return this.render();
    }
}

export default Component;
```

As you can see, our component class has two instance methods: the `render` method, that uses an instance variable called `template` to "render" the component, and a `toString` method, which overrides the default `toString` in order to return the result of the `render` method. This "templating" approach is extremely simplistic and should not be used - _ever_ - but templating is not the focus of this tutorial!

Consider this piece of code:

```javascript
return this.template.replace(
        /\{[a-z0-9\-_]+}/ig,
        v => {
            let value = this[v.substring(1, v.length - 1)];
            if (!value)
                return;

            if (value.constructor === Array) {
                return value.join(' ');
            }

            return value;
        }
);
```

Coming from ES5, this might look a little bit alien. While the first parameter should be straighforward (a RegEX pattern), the second one is nothing more than a callback, written with ES6's [_arrow notation_](http://babeljs.io/docs/learn-es2015/#arrows-and-lexical-this).

> In ES6, a function can be written without the `function` keyword. The syntax used is the _arrow_ syntax, so writing this:
```
function sum(a, b) { return a + b; }
```
is mostly equivalent to this:
```
let sum = (a, b) => a + b;
```
The main differences are that the arrow notation automatically binds the `this` keyword within the function's scope to the parent's scope `this`, and that a return statement can be omitted when returning the result of the first operation within the function.

What the function does is this:

- It looks for something matching the pattern in the component's template. The pattern is `{variableName}`;
- If it finds something, it tries to get `variableName` from the component's instance variables;
- If it finds it and it's an array, it joins the elements of the array into a string, separating each one with a single space character;
- It returns either the variable's value or the concatenated string.

Since we've overriden the `toString` method on the component, any time a the value of the instance variable is another component, this gets automatically rendered into a string. This way we can recursively render nested components.

Having defined how a _Component_ works, we can now review our _Application_ class code:

```javascript
import jQuery from 'jquery';

import Component from './Component';

class Application extends Component {
    constructor() {
        super();

        this.template = require('html!templates/main.html');
    }

    start(selector) {
        let container = document.querySelector(selector);
        if (!container) {
            throw new Error(`Container ${selector} not found!`);
        }   

        this.container = container;     
        this.messages = [];

        this.refresh();
    }

    refresh() {
        jQuery(this.container).html(this.render());
    }
}

export default Application;
```

In the code above, we _imported_ the `Component` class and made the `Application` class extend it. Notice how the constructor now calls the `super` method: this is _mandatory_ whenever a class extends another and you want to do override the constructor.

We also load the content of an HTML file inside the `template` instance variable. In order for this to work, we need to install a new dependency and slightly alter our `webpack.config.js`:

```bash
npm install html-loader --save-dev
```

```javascript
module.exports = {
    ...
    module: {
        loaders: [
            // We explicitly state which file is to be processed by the file-loader
            { test: /index\.html/, loader: "file?name=[name].[ext]" },
			{
				test: /\.js?$/,
				exclude: /(node_modules)/,
				loader: 'babel',
				query: {
					presets: ['es2015']
				}
			}       
        ]  
    },
    ...
}
```

This modification is necessary because the `file-loader` doesn't actually _read_ the contents of the required file but limits itself to copying it to the bundle folder, while the `html-loader` allows Webpack to actually inject the contents of an html file inside a variable when requiring it. Also note that, when requiring an html file, we prepend the filename with the `html!` notation: this tells Webpack which loader to use when requiring the file.

Our template file is actually quite simple, containing only a form and a container where the list of messages is going to be displayed:

```html
<div class="container">
    <h1 class="text-center">Notice Board</h1>
    <div class="panel">
        <div class="panel-body">
            <form id="new-message-form">
            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">
                        Author
                    </span>
                    <input type="text" name="author" class="form-control">
                </div>
            </div>

            <div class="form-group">
                <div class="input-group">
                    <span class="input-group-addon">
                        Message
                    </span>
                    <input type="text" name="text" class="form-control">
                </div>
            </div>

            <button type="submit" class="btn btn-success">Post</button>
            </div>
        </form>    
    </div>

    <div id="messages">{messages}</div>    
</div>
```

## 2.6. Making AJAX calls

Now that we have created the 'backbone' of our application, we need another vital component: some logic to fetch the messages from the server and to post new messages. In order to achieve this, we're going to rely on another dependency, since we don't really want to reinvent the wheel.

```bash
npm install axios --save
```

The _[axios](https://github.com/mzabriskie/axios)_ library is a Promise-based HTTP client that allows us to make asynchronous HTTP requests towards any server from our browser (or from NodeJS, if we wish to do so). It does pretty much what `jQuery.ajax` or a native `XHR` does, just in a more structured (and Promise A/A+ compliant) way. [Promises](http://babeljs.io/docs/learn-es2015/#promises) are a built-in feature of ES6 and, even though most browsers support them, you should use a [polyfill](http://babeljs.io/docs/usage/polyfill/) if you want to support older browsers.

> _axios_ is an excellent example of _isomorphic_ JavaScript. Isomorphic code is written in such a way that it can run both on the client and the server. This concept is taken even further by some libraries such as ReactJS or Angular2, that allow for _entire applications_ to be written so that they can run either on the client or on the server without issue.

Let's use axios to fetch all the messages from the server when our application starts:

```javascript
import jQuery from 'jquery';
import http from 'axios';

import Message from './Message';
import Component from './Component';

class Application extends Component {
    constructor() {
        super();

        this.template = require('html!templates/main.html');
    }

    start(selector) {
        let container = document.querySelector(selector);
        if (!container) {
            throw new Error(`Container ${selector} not found!`);
        }   

        this.container = container;     
        this.messages = [];

        this.fetchMessages();
    }

    refresh() {
        jQuery(this.container).html(this.render());
    }

    fetchMessages() {
        console.log('Downloading messages from server...');

        http.get('http://localhost:3000/posts')
            .then(
                response => {                    
                    this.messages = response.data.data.map(m => new Message(m)).reverse() || [];

                    this.refresh();
                },

                error => {
                    console.log(error);
                }
            )
    }
}

export default Application;
```

We added a `fetchMessages` method to the `Application` class, and within it called the `get` static method on the `http` (which is how we decided to call _axios_ within our application) object. Since the `get` method (just as most other methods defined in _axios_) returns a valid promise, we chained a call to the `then` function, passing to it two callbacks: one to be called when the request succeeds, and another to be called in the event that the request fails. We defined both this callbacks as arrow functions, to leverage the `this` variable scope binding to the `Application` instance.

## 2.7. Destructuring

When the request to the server succeeds, the Application object's `messages` instance variable is populated with a number of `Message` objects. This object is a component, just like Application, and this is how its template looks like:

```html
<div class="panel panel-default">
    <div class="panel-body">
        {text}
    </div>
    <div class="panel-footer text-right">
        Posted by <strong>{author}</strong> on <em>{date}</em>
    </div>
</div>
```

Its class definition, however, looks like this:

```javascript
import Component from './Component';
import moment from 'moment';

class Message extends Component {
    constructor({author, text, date}) {
        super();

        let formattedDate = moment(new Date(date));

        this.author = author;
        this.date = formattedDate.format('MMMM Do YYYY, hh:mm:ss');
        this.text = text;

        this.template = require('html!templates/message.html');
    }
}

export default Message;
```

The thing that probably is most confusing is the `constructor` method declaration:

```javascript
constructor({author, text, date}) { /* ... */ }
```

This strange syntax is the application of another ES6 feature: [_destructuring_](http://babeljs.io/docs/learn-es2015/#destructuring).

Consider a simple JavaScript object:

```javascript
let message = {
    author: "James",
    text: "Hello!"   
}
```

ES6 allows us to 'destructure' this object into variables, or to save the content of each key into a variable with the same name, using the following syntax:

```javascript
let { author, text } = message;
```

The same feature can be used to define the parameters of a function, stating that it will receive an object and will destructure it into the specified variables, that will be then made available to function's scope. This is what the constructor of the `Message` class does!

> Note that destructuring an object does not require to specify *all* the keys: those that are not specified are simply going to be ignored:
```javascript
let object = {a: 1, b: 2, c: 3, d: 4};
let { a, c } = object;
```

The message constructor also uses the excellent [_moment.js_](http://momentjs.com/) library to manipulate and format date objects/strings:

```bash
npm install moment;
```

## 2.8. Completing the Application

Now that we have also defined the _Message_ component, we need to put some finishing touches to our application:

- Post new messages to the server
- Bootstrap the application
- Include a stylesheet

In order to post new messages, we simply need to make a post request to the server, and bind the form to a function that initiates such operation. We can do this by modifying our `Application.js`:

```javascript
import jQuery from 'jquery';
import http from 'axios';

import Component from 'modules/Component';
import Message from 'modules/Message';

class Application extends Component {
    constructor() {
        super();

        this.template = require('html!templates/main.html');
        console.log(this.template);
    }

    start(selector) {
        let container = document.querySelector(selector);
        if (!container) {
            throw new Error(`Container ${selector} not found!`);
        }   

        this.container = container;     
        this.messages = [];

        this.fetchMessages();
    }

    refresh() {
        jQuery(this.container).html(this.render());

        jQuery('#new-message-form', this.container)
            .on('submit', (event) => {
                event.preventDefault();

                let author = jQuery('[name=author]', '#new-message-form').val();
                let text = jQuery('[name=text]', '#new-message-form').val();

                this.postMessage(author, text);

                return false;
            });
    }

    postMessage(author, text) {
        console.log('Posting new message to the server...');

        if (!author || !text)
            return;

        http.post('http://localhost:3000/posts', {author, text})
            .then(
                response => {
                    let result = response.data;

                    this.messages = [new Message(result.data), ...this.messages].reverse();
                    this.refresh();
                },
                error => {
                    console.log(error);
                }
            )
    }

    fetchMessages() {
        console.log('Downloading messages from server...');

        http.get('http://localhost:3000/posts')
            .then(
                response => {                    
                    this.messages = response.data.data.map(d => new Message(d)).reverse() || [];

                    this.refresh();
                },

                error => {
                    console.log(error);
                }
            )
    }
}

export default Application;
```

We added the `postMessage` instance method and binded a callback to the `submit` event of the form that retrieves the values from the input fields and passes them to the `postMessage` method.

One thing you might notice is this line of code within the `success` callback of the `postMessage` promise:

```javascript
this.messages = [new Message(result.data), ...this.messages];
```

Here we are using another feature of ES6: the [_spread operator_](http://babeljs.io/docs/learn-es2015/#default-rest-spread). This operator allows you to "spread" the contents of an array into a number of variables equal to the number of the elements within the array or "collect" a number of variables into a single array, depending on where you use it. In this case, we want to *add* an element to the head of our already existing `messages` array - which could just as easily be done by calling `unshift` - so we are creating a copy containing the new Message and the "spreaded" contents of the old array.

What's left is to bootstrap the application. We can go back to our `index.js` file within our `src` folder and modify it so that it looks like this:

```javascript
import Application from 'modules/Application';

let app = new Application();
app.start('#main');
```

That was easy, wasn't it?

One of the last finishing touches we might want to make to the application is to add a stylesheet. Since we don't want to write a line of CSS for this tutorial, we're going to include Twitter Bootstrap:

```bash
npm install bootstrap --save
```

Since we're using Webpack, and Webpack can function with almost any kind of file extension, we're going to require the CSS just like any other dependency - from within the code. Let's modify our `index.js` one last time:

```javascript
global.jQuery = require('jquery');
require('bootstrap/dist/js/bootstrap.js');
require('bootstrap/dist/css/bootstrap.css');

import Application from 'modules/Application';

let app = new Application();
app.start('#main');
```

Other than requiring both the CSS and the JS from the Bootstrap library, we also defined jQuery as a global variable. While polluting the global namespace is never a good thing, this is one of those cases where it's necessary since Bootstrap expects to find jQuery in the global scope.

Were we to fire up Webpack right now, we would get countless errors, mostly due to the fact that Webpack doesn't know how to treat some font files referenced from within Bootstrap. We can solve this issue by mapping those extensions to the appropriate loader from within the `webpack.config.js` file:

```javascript
module.exports = {
    ...
    module: {
        loaders: [
            { test: /index\.html/, loader: "file?name=[name].[ext]" },
			{
				test: /\.js?$/,
				exclude: /(node_modules)/,
				loader: 'babel',
				query: {
					presets: ['es2015']
				}
			},
            { test: /\.css$/, loader: "style-loader!css-loader!autoprefixer-loader" },
            { test: /\.woff2?(\?v=\d+\.\d+\.\d+)?$/,   loader: "url?limit=10000&mimetype=application/font-woff" },
            { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=application/octet-stream" },
            { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    loader: "file" },
            { test: /\.png(\?v=\d+\.\d+\.\d+)?$/,    loader: "file" },
            { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=image/svg+xml" },       
        ]  
    },
    ...
}
```

We also need to install a couple more development dependencies:

- `style-loader` and `css-loader` allow us to load stylesheets
- `autoprefixer-loader` autoprefixes certain CSS properties with vendor-specific prefixes (such as `-moz`, `-ms`, `-webkit`)
- `url-loader` works just like the file loader but returns a [data url](http://dataurl.net/#about) instead of a path if the size of the file it's parsing is smaller than the specified limit. This allows us to embed certain, smaller dependencies directly into our code, thus requiring less HTTP requests on page-load.

That's it! Running `webpack` now should bundle the application, which is now ready to use!

## 2.9. Live reloading development server

When developing web applications it can become tedious to reload constantly every time we make a modification to our codebase. Fortunately, this problem has been solved for us, and we are one dependency away from live reloading:

```bash
npm install -g webpack-dev-server
```

This installs `webpack-dev-server` _globally_, meaning it can be run from anywhere. Running it _instead_ of `webpack` will start a live development server that will automatically serve the bundled content of your application on `http://localhost:8080` (by default). It will also automatically repack everything everytime a modification is made to the code, attempting to speed up the process by working only on the modified parts and leaving alone the untouched files.

> Some browsers, such as Google Chrome, do not allow XHR requests when opening HTML files locally using the `file://` protocol. This makes the deployment of the application on a server mandatory, even during development. `webpack-dev-server` perfectly satisfies this requirement.
