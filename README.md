# Modern Javascript Tutorial

## Contents

1. Build Tools
    1. Setting up your environment  
    1. Creating a new project
	1. Setting up Webpack
	1. Loaders
	1. Plugins
	1. Debugging

1. Javascript ES6 (or ES2015, or Javascript 1.7)  
    1. Introduction
	1. Webpack Configuration
    1. The Server Application
	1. Classes and Modules
	1. Writing functions, the ES6 way
	1. Making AJAX calls
    1. Destructuring
    1. Completing the Application
    1. Live reloading development server

1. AngularJS (1.4)  
    1. Introduction
    1. Creating an AngularJS application
    1. Routing
    1. Controllers
    1. Views
    1. Notice Board Application
    1. Services
    1. Resources
    1. Directives
    1. Filters

1. ReactJS
    1. Introduction
    1. The JSX Syntax
    1. Configuring Webpack
    1. Hello, React!
    1. Stateless and Stateful Components
    1. The Notice Board Application
    1. Forms and component refs
    1. Lists!

1. Redux
    1. Introduction
    1. The three principles of Redux
    1. State
    1. Actions
    1. Reducers
    1. Notice Board Application
    1. Asynchronous actions
    1. Connecting React to Redux
    1. Dispatching Actions
    1. Posting New Messages
