let express = require('express');
let bodyParser = require('body-parser');
let cors = require('cors');
let app = express();
let posts = [];

app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/posts', (req, res) => {
    return res.status(200).json({
        status: 'OK',
        data: posts
    });
});

app.post('/posts', (req, res) => {
    if (!req.body || !req.body.author || !req.body.text) {
        return res.json({
            status: 'ERROR',
            data: '`author` and `text` fields are required'
        });
    }

    let message = {
        author: req.body.author,
        text: req.body.text,
        date: new Date()
    };

    posts.push(message);

    console.log(`Added message from ${message.author}`);

    return res.send({
        status: 'OK',
        data: message
    });
});

app.listen(3000, () => {
    console.log('Server started on port 3000');
});
