module.exports = {
    context: __dirname + '/src',
    entry: './index.js',
    output: {
        path: __dirname + '/dist',
        filename: 'index.js'
    },
    target: 'node',
    module: {
        loaders: [
            { test:  /\.json$/, loader: 'json-loader' },
			{
				test: /\.js?$/,
				exclude: /(node_modules)/,
				loader: 'babel', 
				query: {
					presets: ['es2015']
				}
			}         
        ]  
    },
	devtool: 'source-map'
}