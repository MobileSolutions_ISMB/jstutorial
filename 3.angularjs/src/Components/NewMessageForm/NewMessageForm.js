import angular from 'angular';

import NewMessageController from './NewMessageController';

function NewMessageForm() {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'NewMessageController',
        controllerAs: 'ctrl',
        template: require('html!./new-message-form.html'),
        scope: {
            onPost: '&'
        }
    }
}

export default angular.module('directives.newMessageForm', [])
    .controller(NewMessageController.name, NewMessageController)
    .directive('newMessageForm', NewMessageForm)
    .name;
