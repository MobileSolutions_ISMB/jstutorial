class NewMessageController {
    constructor() {
        this.resetMessage();
    }

    resetMessage() {
        this.newMessage = {
            author: '',
            text: ''
        };
    }

    postMessage() {
        this.onPost({message: this.newMessage});

        this.resetMessage();
    }
}

NewMessageController.$inject = [];

export default NewMessageController;
