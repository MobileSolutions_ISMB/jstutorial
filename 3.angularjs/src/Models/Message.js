function Message($resource) {
    return $resource('http://localhost:3000/posts');
}

Message.$inject = ['$resource'];

export default Message;
