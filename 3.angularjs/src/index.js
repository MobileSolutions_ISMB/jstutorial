global.jQuery = require('jquery');

require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/js/bootstrap.js');

import angular from 'angular';
import uirouter from 'angular-ui-router';
import resource from 'angular-resource';
import angularMoment from 'angular-moment';

import routes from 'Config/routes';

import HomeController from 'Controllers/HomeController';

import Message from 'Models/Message';

import NewMessageForm from 'Components/NewMessageForm/NewMessageForm';

angular.module('app', [uirouter, resource, angularMoment.name, NewMessageForm])
    .config(routes)
    .factory(Message.name, Message)
    .controller(HomeController.name, HomeController);
