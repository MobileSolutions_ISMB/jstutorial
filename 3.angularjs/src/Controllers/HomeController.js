class HomeController {
    constructor(Message) {
        this.Message = Message;

        this.fetchMessages();
    }

    postMessage(message) {
        this.Message.save(message).$promise
            .then(
                response => {
                    this.fetchMessages();
                },
                error => {
                    console.log(error);
                }
            )
    }

    fetchMessages() {
        this.Message.get().$promise
            .then(
                response => {
                    this.messages = response.data.reverse();
                },
                error => {
                    console.log(error);
                }
            );
    }
}

HomeController.$inject = ['Message'];

export default HomeController;
