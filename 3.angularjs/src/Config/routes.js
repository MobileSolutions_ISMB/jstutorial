function routes($urlRouterProvider, $locationProvider, $stateProvider) {
    $locationProvider.html5Mode(true);
	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('home', {
			url: '/',
			template: require('html!Views/home.html'),
			controller: 'HomeController',
			controllerAs: 'ctrl'
		})
}

routes.$inject = ['$urlRouterProvider', '$locationProvider', '$stateProvider'];

export default routes;
