# 3. AngularJS

## 3.1. Introduction

As we've seen in the previous chapters, when combined with a package manager like _npm_ and a bundling tool like _Webpack_ JavaScript becomes a powerful language. Still, writing full applications in JavaScript can be very difficult if they're not given a proper structure. In the previous chapter we've built a simple _Notice Board_ application without giving too much thought on **how** we were going to do it, since we were only interested in exploring the features of ES6. In this chapter we're going to focus on a framework that gives a precise and _very_ opinionated structure to your applications: [_AngularJS_](https://angularjs.org/)

> Since Angular2 is still in beta at the time of this writing, and so profoundly different from AngularJS, we're going to focus on AngularJS 1.4 - which is extremely stable, mature and production ready.

### What is AngularJS

AngularJS is a *framework*, which is a set of tools put together to allow developers to create applications around a predefined scaffolding. It follows the *MVC* paradigm, which is short for _Model-View-Controller_:

- The **model** represents a _resource_, an entity that is manipulated by your application by being read, written or both. In our Notice Board example, a Message is such an entity, and can therefore be used as Model.
- The **controller** is the business logic behind the scenes: it's a piece of code that can receive actions from the user interface (or something else) and translate them to operations applied to the model - and much more!
- The **view** is the part of the application that faces the user, and is usually represented through an interface that has some level of interactivity. Through this interface the user can trigger actions on the controller, which will in turn do something to the model and update, if needed, the interface to give the user   
some form of feedback.

## 3.2. Creating an AngularJS application

Like in the previous chapters, we'll start with a new folder containing an `src` subfolder and two files: `package.json` and `webpack.config.js`. You can use the last version of `webpack.config.js` from Chapter 2, while you should generate a new `package.json` by running `npm init` and then installing the proper dependencies:

```bash
# Development dependencies
npm install --save-dev autoprefixer-loader babel-core babel-loader babel-preset-es2015 css-loader file-loader html-loader html-webpack-plugin style-loader url-loader webpack

# Application dependencies
npm install --save bootstrap jquery angular angular-ui-router  
```

We're all set! Let's create our two standard entry points inside the `src` folder: `index.html` and `index.js`. The HTML file is going to look very similar to that of the previous chapter:

```html
<!DOCTYPE html>
<html lang="en" ng:app="app">
<head>
    <meta charset="UTF-8">
    <title>Notice Board</title>
    <base href="/">
</head>
<body>
    <ui:view></ui:view>
    <script src="app.js"></script>
</body>
</html>
```

You'll probably notice straight away that something is off: there is a non-standard `ng:app` property on the opening `html` tag, and a non-standard `ui:view` tag in the body! These are a feature of AngularJS called _directives_ and that we'll see later on.

Our `index.js` file will be tasked with including all the ES6 modules that we're going to create for our application, and will also take care of bootstrapping the application itself. Since AngularJS is a complete framework, this is a rather simple operation since it only requires to define a module.

```javascript
global.jQuery = require('jquery');

require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/js/bootstrap.js');

import angular from 'angular';

angular.module('app', []);
```

Let's take a moment to understand how an AngularJS module is defined:

```javascript
angular.module(
    'module-name', // Name
    [ /* Dependencies */ ]
);
```

The `module` method on the `angular` object can receive either one or two parameters, the first _always_ being the name of the module. The second parameter is the list of the dependencies of the module, and must always be an array of `String`s; this parameters is only passed once, when defining the module. As you might have guessed, calling the `module` method without the second parameter allows you to retrieve the module from Angular's internal module registry, allowing you to define different parts of a module in different files.

An Angular application is a module, and is therefore defined as such. Going back to the `index.html` file, you'll notice that the name of our application module, `app`, matches the value passed to the `ng:app` attribute on the `html` tag: this is how we tell angular which module is going to be the entry point of our application!

## 3.3. Routing

Even though this works, it doesn't really do much of anything. In order to be useful our application needs to have some pages, and in order for a page to be reachable by the user, there needs to be a _route_ to it. A _route_ defines what should happen in our application when a certain path is reached. What page needs to be shown, for example, when we reach the "/" address on the application? What is the underlying logic of that page?

In order to define routes we are going to need an external module and we're going to use [_angular-ui-router_](https://github.com/angular-ui/ui-router), which is **not** part on the AngularJS core but a third-party package with better support for complex routing (which is quite common in complex applications).

We already installed this dependency when setting up our application, so we need to include it in our code:

```javascript
global.jQuery = require('jquery');

require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/js/bootstrap.js');

import angular from 'angular';
import uirouter from 'angular-ui-router';

angular.module('app', [uirouter]);
```
As you can see, the dependency is loaded as an ES6 module and passed to the dependency array in our `app` module definition.

> When passing a dependency to a module, it must be passed as a `String`. The convention for AngularJS modules is to `export` only their name, which is enough for AngularJS to retrieve everything else since they independently register themselves within the Angular module registry when they are loaded.

After importing the module and passing it as a dependency to our module, we need to configure a route. A good practice is to define our route configuration in a dedicated file, which we are going to create inside the `src/Config/routes.js` file.

```javascript
function routes($urlRouterProvider, $locationProvider, $stateProvider) {
    $locationProvider.html5Mode(false);
	$urlRouterProvider.otherwise('/');

	$stateProvider
		.state('home', {
			url: '/',
			template: require('Views/home.html'),
			controller: 'HomeController',
			controllerAs: 'ctrl'
		})
}

routes.$inject = ['$urlRouterProvider', '$locationProvider', '$stateProvider'];

export default routes;
```
This module is exporting a `function`. Please note that this is an ES6 module and **not** an AngularJS module, so it's ok to export a `function` instead of a simple `String`. We'll soon be seeing how we're going to feed this function to Angular, but for now let's focus on what this method does: the first thing you'll notice is that the function itself takes 3 arguments, which are later `$inject`ed through an array as strings: this is to satisfy AngularJS's dependency injection requirements and to make it so that minification doesn't break the code.

> AngularJS uses [_Dependency Injection_](https://en.wikipedia.org/wiki/Dependency_injection). Through this mechanism, Angular is able to automatically assign a value to an argument of a function by the name of the argument itself. Since every dependency that can be injected is registered with a specific name it's necessary to specify the names of the dependencies as `String` values within the `$inject` array, so that when the minification process changes the names of the arguments Angular can still identify the correct dependency through the string within the `$inject` array at the same index as the argument.

Each of the arguments passed to this function represents as _Provider_. Providers are special objects that expose global values available throughout the application, which are also configurable when the application starts through a configuration method (such as the `routing` method defined above). In this case, we are using three providers:

- The `$locationProvider` is a built-in AngularJS provider that allows us to manipulate how the URLs appear in the address bar. In this case we are simply telling Angular to use HTML5-style URLs, so URLs with no hashtags or hashbangs;
- The `$urlRouterProvider` is a provider exposed by the `angular-ui-router` package and its function is to monitor the current location and listen to changes. Here, we are simply telling it to fallback to the "/" route whenever an invalid route is found;
- The `$stateProvider` is also exposed by the `angular-ui-router` package and its responsibility is to handle the various routes. This is where we're going to define how our application is going to behave depending on what route we're visiting.

When configuring the `$stateProvider` we can chain together multiple calls to the `state` method. For now, we're going to call it just once to define how the application should behave when reaching the '/' route:

```javascript
$stateProvider
    .state('home', {
        url: '/',
        template: require('Views/home.html'),
        controller: 'HomeController',
        controllerAs: 'ctrl'
    })
```
In order to define a route we should tell Angular at least a couple of things: which template it should render, and which controller it should use to handle the business logic. In this case we are rendering a template located in the `Views/home.html` file (requiring it using Webpack!) and we are relying on the logic defined in the `HomeController`. We'll soon define both, but right now let's see how we should tell Angular how to use this configuration; let's go back to our `index.js` file:

```javascript
global.jQuery = require('jquery');

require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/js/bootstrap.js');

import angular from 'angular';
import uirouter from 'angular-ui-router';

import routes from 'Config/routes';

angular.module('app', [uirouter])
    .config(routes);
```

As you can see, this is quite simple: we `import` our function and feed it to angular an an argument to the `config` method. Angular will take care of running it (passing the correct dependencies) when it bootstraps the application.

## 3.4. Controllers

As a general rule of thumb, whenever you have a route you'll probably going to have a _Controller_ associated to it. A controller is used to manage the business logic that connects a view and one or more models.

Let's create our controller in `src/Controllers/HomeController.js`:

```javascript
class HomeController {
    constructor() {
        this.name = 'Nobody';
    }

    greet() {
        alert(`Hello, ${this.name}`);
    }
}

HomeController.$inject = [];

export default HomeController;
```

This is quite simple, but we should spend a couple of words on it: we have defined both a `constructor` and an instance method, `greet`. The `constructor` follows Angular's dependency injection rules, so anything specified as an argument will be automatically matched by AngularJS with an entity registered with the same name. This is the reason why we have given the `$inject` static variable a value (for now, an empty array). We are also exporting the controller as a class because this is an ES6 module, not an Angular module (even though we *could* define it as a separate Angular module).

In order to have angular recognize it, we're going to modify yet again our `index.js`:

```javascript
global.jQuery = require('jquery');

require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/js/bootstrap.js');

import angular from 'angular';
import uirouter from 'angular-ui-router';

import routing from 'Config/routes';

import HomeController from 'Controllers/HomeController';

angular.module('app', [uirouter])
    .config(routes)
    .controller(HomeController.name, HomeController);
```

Again, we just imported it and passed it to the `controller` method on our module, giving it the name (which is how we refer to it) and the class itself.

## 3.5. Views

All is left to have our `home` route working is something to show to the user. Let's create the _View_ in `Views/home.html`:

```html
<h1>Hello!</h1>
<div class="form-group">
    <label>Type your name</label>
    <input type="text" ng:model="ctrl.name" class="form-control" />
</div>
<div class="form-group text-right">
    <h4 class="pull-left">You have typed: {{ctrl.name}}</h4>
    <button class="btn btn-lg btn-success" ng:click="ctrl.greet()">Say hello!</button>
</div>
```

This looks like a mostly standard HTML file, with the a couple of exceptions: the `ng:model` and `ng:click` attributes of, respectively, the `<input>` and the `<button>` tags; there is also a template-like value in the `<h4>` tag.

`ng:model` is an attribute used by Angular to define a _binding_ between the value of an HTML input and a variable. As you can see in the HTML above, the variable that is being binded to the `<input>` is the `name` variable on the `ctrl` object. What is the `ctrl` object though? Let's go back to the `routes.js` file:

```javascript
$stateProvider
    .state('home', {
        url: '/',
        template: require('html!Views/home.html'),
        controller: 'HomeController',
        controllerAs: 'ctrl'
    })
```

As you can see, when defining the route we specified a `controllerAs` key: this key allows us to refer to the controller object from within the view as a variable with the name specified as the value of the `controllerAs` key. In our case, we decided that the controller would be referred to as the `ctrl` variable, and we can therefore access the `name` variable on the controller through the `ctrl.name` notation.

Angular implements what is called _Two-way data binding_. If we take our example into account, this means that the value of the `input` tag and the `ctrl.name` variable, once binded through the `ng:model` attribute, will _always_ have the same value, no matter if the variable value is changed from within the controller, the input value changes or something else happens. As you might guess, this makes writing forms with Angular very easy, with the drawback of becoming very cumberstone when a large number of elements are binded to variables on the controller.

Whenever you type something into the form, you'll see that the content of the `<h4>` tag changes where the `{{ctrl.name}}` notation is placed: this is two-way data binding in action! You can use the `{{ variable }}` notation to link any variable to the the view, and whenever the variable value changes, the view updates as well.

> Note how we have used two different syntaxes for two-way data binding: in the `input` tag we used an `ng:model` attribute and passed a `String` representing the variable as the value; in the `<h4>` tag we used the mustache notation. This is because the `ng:model` attribute is somehow special, as we'll later see when discussing _Directives_.

Other than binding variables, Angular also allows us to attach functions to events so that we can react accordingly. In our view, for instance, we attached the `ctrl.greet` method to the `click` event of the `<button>`. Whenever we want to attach an event listener to an HTML element, we have to use the `ng:` notation followed by the name of the event. AngularJS supports all standard HTMl5 events.

> You might not like using double columns in your HTML. AngularJS also supports other notation, all equally valid: `ng:model` is equivalent to `ng-model` and `data-ng-model`. Use the one that best suits your coding style!

## 3.6. Notice Board Application

Now that we have seen how to make an Hello world, let's remake the Notice Board application. We haven't written too much code, and in fact most of it won't have to change.

Let's change our `home.html` to this:

```html
<h1>Notice Board</h1>

<!-- The new message form will go here -->
<hr>

<div class="panel panel-default" ng:repeat="message in ctrl.messages">
    <div class="panel-body">
        {{message.text}}
    </div>
    <div class="panel-footer text-right">
        Written by <strong>{{message.author}}</strong> on <em>{{message.date}}</em>
    </div>
</div>
```

Our new home will show a list of messages through the use of the very special AngularJS attribute `ng:repeat` which, as you might have guessed, repeats the HTML element to which it's attached as many times as there are elements in the array specified within its value. The syntax for `ng:repeat` can become quite complex, but its most simple form is

```html
<div ng:repeat="element in array"></div>
```

Whenever you define a repeat directive, you get to decide how to refer to each element of the array, so that you can use that variable name in the HTML element you are repeating (and its children) to render the correct values. In this case, we referred to each message as `message`, and printed the various variables within each message inside the rendered HTML.

## 3.7. Services

Our application is lacking content. As luck would have it, we have a RESTful API we can consume in order to fetch messages! Let's go back to our `HomeController`:

```javascript
class HomeController {
    constructor($http) {
        this.$http = $http;

        this.fetchMessages();
    }

    fetchMessages() {
        this.$http.get('http://localhost:3000/posts')
            .then(
                response => {
                    // Reverse messages so that we see the most recent first
                    this.messages = response.data.reverse();
                },
                error => {
                    console.log(error);
                }
            );
    }
}

HomeController.$inject = ['$http'];

export default HomeController;
```

This time, the constructor is receiving a single argument, which is Angular's built-in `$http` service. This service works as HTTP client that allows you to make any kind of request to a server, much in the same way of the `axios` package we used in the previous chapter. In fact, `axios`'s syntax is based on Angular's `$http`!

As you might have noticed, in order for things to work correctly, we are also `$inject`ing the `$http` string - this is to satisfy Angular's dependency injection parser!

The `fetchMessages` method on the `HomeController` does nothing more than to make a GET request and, in case of success, saves the resulting data in the messages instance variable. Since this variable is accessible from within the view, as soon as it's populated we'll see the messages pop up in our application!

In general, a service is a singleton object that, when injected into a controller, provides - as the name itself suggests - a service that the controller itself can consume. A number of built-in services are available and ready to use, and you can easily make your own the same way you would make a controller, simply registering it using the `service` method instead of the `controller` method.

## 3.8. Resources

While making HTTP calls from within a controller is tempting and, in some cases, appropriate, our case is one where it would be better to define a _Model_ and consume it in an opaque way from the Controller. We're going to create a `Message` model in `src/Models/Message.js`:

```javascript
function Message($resource) {
    return $resource('http://localhost:3000/posts');
}

Message.$inject = ['$resource'];

export default Message;
```

AngularJS provides a ready-to-use service called `$resource` that can be used to define models. In its simplest form, it requires only the path to the endpoint on the server so that it can represent the specified resource. It can, of course, be configure to handle much more complex cases.

In order for our model to be consumed, we need to register it on our application module, within `src/index.js`:

```javascript
global.jQuery = require('jquery');

require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/js/bootstrap.js');

import angular from 'angular';
import uirouter from 'angular-ui-router';

import routes from 'Config/routes';

import HomeController from 'Controllers/HomeController';

import Message from 'Models/Message';

angular.module('app', [uirouter])
    .config(routes)
    .factory(Message.name, Message);
    .controller(HomeController.name, HomeController);
```

This time, we used the `factory` method. This method is very similar to the `service` method used to register method, with the only difference that it returns a simple function instead of an instance. If the difference seems subtle, it's because it is. There have been many discussions on the matter, you can read more about it [here](http://blog.thoughtram.io/angular/2015/07/07/service-vs-factory-once-and-for-all.html) - the article actually advocates *against* the use of factories, but in this case it's okay to use them.

How would we go about consuming our resource in our controller? It turns out it's quite simple:

```javascript
class HomeController {
    constructor(Message) {
        this.Message = Message;

        this.fetchMessages();
    }

    fetchMessages() {
        this.Message.get().$promise
            .then(
                response => {
                    this.messages = response.data;
                },
                error => {
                    console.log(error);
                }
            );
    }
}

HomeController.$inject = ['Message'];

export default HomeController;
```

What we did was simply to swap the dependency from the `$http` service to the `Message` resource, and use that to fetch the messages. There are many advantages to using resources over using direct `$http` calls, first and foremost because resources are reusable and can be configured to encapsulate all the actions you would normally do with a model: fetch all items, fetch a specific items, create a new item, update an existing one or delete one.

The only catch when using resources is that the underlying `$promise` object must be referenced directly in order to use the `then` method. Otherwise, you can use the built-in `success` and `error` methods which are not, however, Promise A/A+ compliant.

## 3.9. Directives

Now that we have our "backbone" in place, we need one more thing to complete the application: a new message form, and something to post the new message to the server.

One of the most peculiar features of AngularJS are _Directives_. A directive is what more modern frameworks refer to as a _Component_: a reusable piece that can be plugged into an application to extend its functionality. The most simple case of a component everyone uses is the button - it's so widely used that it's been in the HTML spec for decades! Of course compoents can also be much more complex, and expose a wide array of different functionalities: some examples might be list views, tabbed views, date pickers and so on.

Our new message form will be a directive. Let's start by creating a new folder, `src/Components/NewMessageForm`, and a new file inside it: `NewMessageForm.js`:

```javascript
import angular from 'angular';

import NewMessageController from './NewMessageController';

function NewMessageForm() {
    return {
        restrict: 'E',
        bindToController: true,
        controller: 'NewMessageController',
        controllerAs: 'ctrl',
        template: require('html!./new-message-form.html'),
        scope: {
            onPost: '&'
        }
    }
}

export default angular.module('directives.newMessageForm', [])
    .controller(NewMessageController.name, NewMessageController)
    .directive('newMessageForm', NewMessageForm)
    .name;
```

Let's take a look: the most important part of the code above is the `NewMessageForm` function, which we use to define the directive. What the function does is return an object that describes the directive, using the following properties:

- `restrict`: it's used to define if the directive is and _element_ (E), and attribute (A), a class (C) or a comment (M). This can, of course, be combined and the property can be omitted altogether if your directive doesn't need to be restricted to a specific type;
- `bindToController`: required when using the `controllerAs` syntax;
- `scope`: specifies which attributes the directive will get from the HTML where it's instanced and how it will bind their values to its own scope. An attribute can be binded as a reference (`&`), a simple string (`@`) or via two-way data binding (`=`). You can also specify the name of the attribute in case you want the internal variable name to be called differently from the actual attribute, for example `onPost: '&onNewMessagePost'`.

All the other properties are the same used when defining a route, and have the same meaning.

The directive is not being defined as part of our `app` module; instead, we are defining a _new_ module, called `directives.newMessageForm`, and we are declaring a _Controller_ and ad _Directive_ inside of it. Instead of the module itself, we are just exporting its name - as it's standard for AngularJS modules.

In our code above, we are importing an external controller, that we are going to create in the same folder as the directive definition file: `Components/NewMessageForm/NewMessageController`:

```javascript
class NewMessageController {
    constructor() {
        this.resetMessage();
    }

    resetMessage() {
        this.newMessage = {
            author: '',
            text: ''
        };
    }

    postMessage() {
        this.onPost({message: this.newMessage});

        this.resetMessage();
    }
}

NewMessageController.$inject = [];

export default NewMessageController;
```
The controller does a very simple thing: stores the data about the new message that is being inserted in the view, and calls the `onPost` callback passing it this data when the form is submitted. The form is, of course, stored in the `Components/NewMessageForm/new-message-form.html` file:

```html
<form ng:submit="ctrl.postMessage()" novalidate>
    <div class="form-group">
        <label>Author</label>
        <input type="text" ng:model="ctrl.newMessage.author" class="form-control" />
    </div>

    <div class="form-group">
        <label>Message</label>
        <input type="text" ng:model="ctrl.newMessage.text" class="form-control" />
    </div>

    <div class="form-group text-right">
        <button type="submit" class="btn btn-success">Send!</button>
    </div>
</form>
```

Now that the directive is defined, we just need to set it as a dependency of our application and include it in our `home.html` file. First, we modify the `index.js`:

```javascript
global.jQuery = require('jquery');

require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/js/bootstrap.js');

import angular from 'angular';
import uirouter from 'angular-ui-router';
import resource from 'angular-resource';

import routes from 'Config/routes';

import HomeController from 'Controllers/HomeController';

import Message from 'Models/Message';

import NewMessageForm from 'Components/NewMessageForm/NewMessageForm';

angular.module('app', [uirouter, resource, NewMessageForm])
    .config(routes)
    .factory(Message.name, Message)
    .controller(HomeController.name, HomeController);
```

Since the directive has been defined as a separate module, after importing the module definition from `NewMessageForm`, we are passing it as a module dependency for our `app`, just like `uirouter` and `resource`.

After including the directive as a dependency, all that is left to do is to use it inside our `home.html` file, binding the `onPost` attribute to a function that will take care of posting the new message to the server:

```html
<h1>Notice Board</h1>

<new-message-form on:post="ctrl.postMessage(message)"></new-message-form>

<hr>

<div class="panel panel-default" ng:repeat="message in ctrl.messages">
    <div class="panel-body">
        {{message.text}}
    </div>
    <div class="panel-footer text-right">
        Written by <strong>{{message.author}}</strong> on <em>{{message.date}}</em>
    </div>
</div>
```
And in our `HomeController.js`:

```javascript
class HomeController {
    constructor(Message) {
        this.Message = Message;

        this.fetchMessages();
    }

    postMessage(message) {
        this.Message.save(message).$promise
            .then(
                response => {
                    this.fetchMessages();
                },
                error => {
                    console.log(error);
                }
            )
    }

    fetchMessages() {
        this.Message.get().$promise
            .then(
                response => {
                    this.messages = response.data.reverse();
                },
                error => {
                    console.log(error);
                }
            );
    }
}

HomeController.$inject = ['Message'];

export default HomeController;
```

And it's done! The `postMessage` function receives a `message` object as its input, and uses the `Message` model to create a new message and send it to the server. When the request is done, it simply fetches all the messages from the server, and Angular will take care of updating the UI.

## 3.10. Filters

While our application is complete, there is still a small touch we can add. The dates are shown in a format that is not very readable, and we have already seen in the previous chapter how an external library, _momentjs_, can be used to solve such an issue. While you could use the library as it is, it just so happens that _momentjs_, like many other libraries, has AngularJS bindings through another small library called _angolar-moment_. Let's install that:

```bash
npm install --save angular-moment
```
This command will install _angular-moment_ as well as _momentjs_, which is listed as its dependency.

After installing the library, we should include it in our project by importing it in `index.js` and pass it as a dependency of our `app` module:

```javascript
global.jQuery = require('jquery');

require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/js/bootstrap.js');

import angular from 'angular';
import uirouter from 'angular-ui-router';
import resource from 'angular-resource';
import angularMoment from 'angular-moment';

import routes from 'Config/routes';

import HomeController from 'Controllers/HomeController';

import Message from 'Models/Message';

import NewMessageForm from 'Components/NewMessageForm/NewMessageForm';

angular.module('app', [uirouter, resource, angularMoment.name, NewMessageForm])
    .config(routes)
    .factory(Message.name, Message)
    .controller(HomeController.name, HomeController);
```

> Note that we are adding `angularMoment.name` instead of just `angularMoment`. This is because the library doesn't export the name, but the whole module. Sometimes this happens, and if strange errors pop up when trying to include a library, this might be the reason.

How are we to use it, though? The library exposes what in Angular jargon is called a _Filter_. Filters provide facilities to manipulate data in order to facilitate its visualization. There are build in filters that allow you, for example, to order the elements in an `ng-repeat` directive, or filter out those you don't want to show (to satisfy a search query in real time, for example).

Let's modify our `home.html` to integrate the filter:

```html
<h1>Notice Board</h1>

<new-message-form on:post="ctrl.postMessage(message)"></new-message-form>

<hr>

<div class="panel panel-default" ng:repeat="message in ctrl.messages">
    <div class="panel-body">
        {{message.text}}
    </div>
    <div class="panel-footer text-right">
        Written by <strong>{{message.author}}</strong> on <em>{{message.date | amDateFormat: 'YYYY/MM/DD HH:mm'}}</em>
    </div>
</div>
```

And that's it! If you run you application now, you should see the dates properly formatted.
