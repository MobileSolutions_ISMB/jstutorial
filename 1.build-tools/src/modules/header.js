var jQuery = require('jquery');

var Header = function(selector) {
    this.$element = jQuery(selector);
    if (this.$element.length === 0) {
        throw new Error('Invalid selector specified');   
    }   
}  

Header.prototype.setText = function(text) {
	var heading = jQuery('h1', this.$element);
    if (heading.length === 0) {
		heading = jQuery('<h1>');
	}
    
    heading.text(text);
    this.$element.append(heading);
}

module.exports = Header;