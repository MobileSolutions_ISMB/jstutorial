var jQuery = require('jquery');
var Header = require('./modules/header.js');

var pageHeader = new Header('#main');
var handleHeaderTextChange = function() {
	var newText = jQuery('input[name=header-title]').val();
	
	pageHeader.setText(newText);	
}

jQuery('#change-title').on('click', handleHeaderTextChange);