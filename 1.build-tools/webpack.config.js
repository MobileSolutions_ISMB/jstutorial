var webpack = require('webpack');
var path = require('path');

var plugins = [];
if (process.argv.indexOf('--optimize') !== -1) {
	console.log('Optimizing code for production...');
	plugins.push(new webpack.optimize.UglifyJsPlugin());
}

module.exports = {
    context: path.join(__dirname, 'src'),
    entry: {
        html: './index.html',
        javascript: './index.js'
    },
    output: {
        path: path.join(__dirname, 'dist'),
        filename: 'app.js'
    },
    module: {
        loaders: [
            { test: /\.html$/, loader: "file?name=[name].[ext]" }
        ]
    },
	devtool: 'source-map',
	plugins: plugins
}
