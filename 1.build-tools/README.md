# 1. Build Tools

## 1.1. Setting up your environment

The first thing you're going to want to have, if you didn't already, is a working installation of NodeJS. You can either download the latest version from the [official site](https://nodejs.org) or use the excellent [nvm](https://github.com/creationix/nvm) by **creationix** to manage your machine's NodeJS installations (and have more than one version installed at the same time!).  
For this book, we're going to adopt the latter solution. As for the version of NodeJS, we'll use v.4.2 LTS. Please note that most of the tools used in the book are going to work with v5.x and should be backwards compatible with v0.11, so feel free to test them out with different versions.

In order to install **nvm** you're going to have to upen up a terminal. We're going to use the terminal *a lot*, so set it up to your liking and make yourself comfortable with it. [This article](http://jilles.me/badassify-your-terminal-and-shell/) should help you get started.

> **A note for Windows users**: I strongly encourage you to install [Git for Windows](https://git-scm.com/downloads), since it comes prepackaged with Git Bash. Since _nvm_ is not supported on Windows, you can use the [4.x binary installer](https://nodejs.org/en/) from the NodeJS Website.

In order to correctly set up your project's development environment, some dependencies (CLI applications and libraries) are needed. In order to streamline the installation of our project's dependencies, we're going to use Node's built-in package manager, `npm`.

> `npm` is a **package manager**. Its purpose is to download software from the internet and place it in a special folder within your project's directory structure. This has several advantages: first, you're not going to have to dig through countless web pages to find the library you're looking for; second, your preferred version can be saved, and automatically redownloaded whenever you need it; third, since your dependencies are saved, you don't have to bloat your source control repository with third party code: a simple file specifying which third-party libraries (and which versions) are required for your software to run is sufficient, helping to keep the the project structure *modular*.

Some of the aforementioned dependencies are going to be installed globally, since they expose some CLI tools that are useful for most projects; others are installed locally on a project-by-project basis. The main tools for the job are:

- **Webpack**: installed both globally and locally it handles the bundling of your project, allowing you to write modular code and use NodeJS packages regardless of where you're going to run your code: the output is going to work both on the server and in the browser.

- **Babel**: one of the most popular transpilers, Babel "translates" your code to something that is supposed to run on every environment by turning it to the most commonly supported format. We'll be using it to transpile ES6/ES7 and React (JSX) code to plain ES5 Javascript code.

- **Tape**: a simple yet effective testing utility with a minimal but powerful API.

- **node-inspector**: a debugging tool that allows you to inspect your code. Useful to debug code that is supposed to run on the server

- **webpack-dev-server**: another developer tool used to set up a live development webserver and serve your project files to the browser. Commonly used to debug client-side code, it supports extremely useful and time-saving features such as live reloading and hot reloading (more on this later on).

In order to install npm packages globally, run this in your terminal (regardless of the folder you're in):

````bash
npm install --global webpack webpack-dev-server babel node-inspector tape
````
> **NOTE:** running `npm install` with the `--global` (or `-g`) flag installs the packages globally. This means that if the package comes with executable files, they are going to be available system wide. Using `npm install` with the `--save` or with the `--save-dev` flag, on the other hand, saves the dependencies (or the development dependencies) in the `package.json` file of the project you're currently working on. In order to correctly save a project's dependencies you need to be in a directory contained within the project's tree.

## 1.2. Creating a new project

A typical application is composed of the following components:
- A source directory, containing all the source files of the application;
- A distribution directory, containing the bundled, minified version of the application (ready for distribution);
- A tests directory, containing the tests required for the application to be considered in a working state;
- A vendors directory, containing all the third party libraries used by the application. This folder, when using `npm`, is usually called `node_modules`;
- Some configuration files, usually placed in the root folder.

Following this very simple list, the directory structure of our project could be this:

    /
      + src
      + dist
      + tests

Note that there is no `node_modules` folder: this is because we have yet to install external modules!

After creating the folder structure, we are going to initialize our project by running

```bash
npm init
```

`npm` will ask us, interactively, some questions about our project. Not all fields are required, and most fields are going to be precompiled. Fill them out however you like, but keep in mind the following considerations:

- You might, one day, want to publish your project inside a public repository. Choose you name so that it doesn't clash with an already existing project!
- Semantic versioning is **VERY** important and you **SHOULD** follow it. Actually, you have no good excuse not to. Read more about semantic versioning [here](http://semver.org/);
- You should always provide a minimal README.md file with your project. Documenting a project is boring and time consuming, but reading someone else's code with no documentation whatsoever can be worse!
- You should really use some form of source control. Think of how happy your future self is going to be if you didn't backup your code when your hard drive dies without warning!
- [Read](https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses) about the different types of licenses and choose the one that best suits you; ISC and MIT licenses are usually good, but you might want to be more restrictive, that's up to you. A more friendly, plain english website about software licenses [is this one](https://tldrlegal.com/).

When you're done with `npm init`, your project will have a new file in its root called `package.json`, containing exactly all the information you've given to the interactive process. This file is also going to store all the dependencies of your project, so this might be a good time to install those. Starting with the development dependencies, run

```bash
npm install --save-dev webpack
```

after some downloading and possibly some compiling, your `package.json` file is going to be updated. You'll also note that you now have a `node_modules` folder. Now it's time for the project dependencies. Let's install jQuery:

```bash
npm install --save jquery
```
Good, now we have all that we need to start coding!

> **NOTE:** your `package.json` file keeps track of the versions of the libraries you've installed for this project. Unless you modify it, you're always going to get the most similar version to the one specified in your package file - usually the most recent minor release - every time you reinstall your dependencies. You can test this by deleting the `node_modules` folder and running `npm install` again.

## 1.3. Setting up Webpack

Webpack is configured by creating a configuration file named `webpack.config.js` in the root of your project. This file will contain all that is required to _bundle_ your project or, in simpler terms, create a minified, optimized version of your application that can be distributed. A minimal webpack configuration looks like this:

```javascript
module.exports = {
    context: path.join(__dirname, 'src'),
    entry: './index.js',
    output: {
        path: __dirname + '/dist',
        filename: 'app.js'
    }
}
```

If you're familiar with NodeJS, you'll probably notice right away that `webpack.config.js` is nothing more than a NodeJS module that exports a plain JS Object. That's because **this is** a NodeJS module! You can even place some logic inside of it, use callbacks instead of values and require NodeJS modules to your heart's desire!

What is it that this configuration file is exporting, though?

- A **context** is the base directory of the project. It gives a custom value to the `.` (dot) in relative paths specified throught the configuration. You can always override it by using absolute paths (usually by prefixing  `__dirname`);
- The **entry** point is the main file of your application;
- The **output** is where your bundled application is going to be saved: it tells Webpack in which directory it should place the files, and how to name the main JS file.

You might be asking yourself: why are both a path and a filename required? This is because Webpack doesn't limit itself to reading and packing Javascript files: it also works very well with JSX, CSS, SCSS, SASS, LESS, HTML, JPEG, PNG and whatever other file extension you might want to include in your project! While Javascript gets minified and bundled in one (or more) files, images and other resources are usually copied into the bundle, thus the directory requirement.

## 1.4. Loaders

In order to support different formats or to change its behaviour with already supported formats, Webpack uses loaders. Loaders are development modules that tell webpack how to behave when it finds references to certain files within your code.

When developing for the frontend, you usually have (at least) a main HTML file and a main JS file. Webpack can happily support multiple entry points and, in fact, an entry point can be either a string, an object or an array. Let's say we want to create a simple page referencing a Javascript file. The HTML for our `index.html` could look like this:

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>My Awesome App</title>
    </head>
    <body>
        <div id="main"></div>
        <script src="app.js"></script>
    </body>
</html>
```

Notice that we are referencing `app.js` instead of `index.js`? This is because our `webpack.config.js` defines the output filename as `app.js`, and the HTML will be copied as-is in the `dist` folder!

We could have our `index.js` perform some trivial task like, for example, printing an "Hello!" header within our `#main` div.

```javascript
(function() {
    var div = document.querySelector('#main');
    if (!div) {
        throw new Error('#main not found!');
    }

    div.innerHTML = '<h1>Hello!</h1>';
})();
```

Now we need to tell webpack two things: which entry points to use (index.html and index.js) and how to behave when it finds an HTML file. The first operation is achieved by simply specifying more than one entry point in the configuration:

```javascript
module.exports = {
    context: path.join(__dirname, 'src'),
    entry: {
        html: './index.html',
        javascript: './index.js'
    },
    output: {
        path: __dirname + '/dist',
        filename: 'app.js'
    }
}
```

For Webpack to recognize an HTML file, we need a loader. HTML files are handled by using the file loader, which is installed by running:

```bash
npm install --save-dev file-loader
```

> Notice the `--save-dev` flag. We are saving this as a _develpoment_ dependencies, something that is only required when developing the application but not when running it. This dependencies is not going to be bundled in the application!

You can now modify your `webpack.config.js` in order to include the loader:

```javascript
module.exports = {
    context: path.join(__dirname, 'src'),
    entry: {
        html: './index.html',
        javascript: './index.js'
    },
    output: {
        path: __dirname + '/dist',
        filename: 'app.js'
    },
    module: {
        loaders: [
            { test: /\.html$/, loader: "file?name=[name].[ext]" }         
        ]  
    }
}
```

Loaders are placed within the `module` section of the configuration, and they are usually definied as a plain object containing a `test` key, which tells Webpack how to parse the file name in order to identify when to use the loader, and a `loader` key, which tells Webpack which loader to use. Loaders can receive queries, allowing you to pass them configuration parameters. You can learn more about loaders by reading the [official Webpack documentation](https://webpack.github.io/docs/loaders.html)

After updating your configuration, running `webpack` from the command line while in the root folder of our project will result in your application getting bundled and saved inside the `dist` folder.

This was not very exciting, was it? Let's try to spice things up a little bit, and create a module. This module will receive a string and a selector, and will print this string in the DOM element associated with the specified selector. Since we decided that jQuery is a dependency of our application, our module will use it.

First we'll create a `modules` folder inside `src`. Within this folder, we'll create a file named `header.js` with the following contents:

```javascript
var jQuery = require('jquery');

var Header = function(selector) {
    this.$element = jQuery(selector);
    if (this.$element.length === 0) {
        throw new Error('Invalid selector specified');   
    }   
}  

Header.prototype.setText = function(text) {
    var heading = jQuery('<h1>');

    heading.text(text);
    this.$element.append(heading);
}

module.exports = Header;
```

Next, we'll update our `index.html` to include a simple text input:

```html
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>My Awesome App</title>
    </head>
    <body>
        <div id="main"></div>
        <div id="input">
            <input type="text" name="header-title" />
            <button id="change-title">Change Title</button>        
        </div>
        <script src="app.js"></script>
    </body>
</html>
```

Next, we'll use our module in our `index.js`:

```javascript
var jQuery = require('jquery');
var Header = require('./modules/header.js');

var pageHeader = new Header('#main');
var handleHeaderTextChange = function() {
	var newText = jQuery('input[name=header-title]').val();

	pageHeader.setText(newText);
}

jQuery('#change-title').on('click', handleHeaderTextChange);
```

If we run the `webpack` command again, our `dist` folder will be updated with a newer version of the bundled code. Opening it will show us a input box and a button, a by using those we'll be able to change the header on the page, creating it if it's not present.

The interesting thing to notice here is that to perform this simple task we have used _modules_. Following the very same principle (and standard!) behind NodeJS module management, we have created a module that _exports_ a function, and we have _required_ this function from somewhere else. Webpack has bundled everything together, making this kind of dependency management available in the browser!

## 1.5. Plugins

In a real world application your build process is probably going to be a little bit more complex than what we have done so far. The amount of customization available is literally endless, and it's impossible to cover every possible scenario. This is why Webpack supports _plugins_. These are, as their name suggests, pieces of code that are plugged into webpack, modifying its behavior during the bundling process.

A very common plugin you're going to use in the real world is the UglifyJS plugin, which will minify and optimize your code to make it production ready. The fact is, you're probably not going to want to use it _every time_, but only when compiling your application for production - the process can take some time and it's not practical when debugging.

There are countless ways to tell webpack _when_ it should do something, and as you might expect, this can be done by adding some code within your `webpack.config.js`. A very naive way would be to pass a specific parameter to webpack to tell it to use the plugin:

```javascript
var webpack = require('webpack');

var plugins = [];
if (process.argv.indexOf('--optimize') !== -1) {
	console.log('Optimizing code for production...');
	plugins.push(new webpack.optimize.UglifyJsPlugin());
}

module.exports = {
    context: path.join(__dirname, 'src'),
    entry: {
        html: './index.html',
        javascript: './index.js'
    },
    output: {
        path: __dirname + '/dist',
        filename: 'app.js'
    },
    module: {
        loaders: [
            { test: /\.html$/, loader: "file?name=[name].[ext]" }         
        ]  
    },
	plugins: plugins
}
```

This way we can call `webpack optimize` whenever we want the code in the `dist` directory to be optimized for prodution. Note that, in order to require a built-in plugin, we have to `require` webpack as a module!

> Note that the UglifyJS plugin is somewhat special, since Webpack _already_ provides specific flags to minimize the code using it. You'll get the same result without modifying `webpack.config.js` and simply running `webpack --optimize-minimize`. This example is only meant to be a proof of concept showing you that you can, in fact, control how and when and how the pluings are loaded!

A list of built-in plugins can be found [here](https://webpack.github.io/docs/list-of-plugins.html). You can also learn how to write your own plugins by following [this guide](https://github.com/webpack/docs/wiki/How-to-write-a-plugin).

## 1.6. Debugging

Debugging is an integral part of the development process and, even if you don't minimize your sources, debugging the bundled that Webpack outputs to our `dist` directory is far from ideal. Fortunately, Webpack supports _sourcemaps_. Sourcemaps have been around for some time, and can be compared to what debug symbols are for compiled applications: mapping between the 'compiled' code and the original source code.

In order to have Webpack output sourcemaps alongside your code, all you have to do is add the following to your `webpack.config.js`:

```javascript
module.exports = {
	...
	devtool: 'source-map',
	...
}
```

Re-run Webpack. You'll now see that a new file has appeared inside your `dist` folder: `app.js.map`. This file is automatically loaded by most browsers, allowing you to place breakpoints and inspect your original code. To see more options available for the `devtool` option key, and to see what other options are available to use with Webpack, check out the [official documentation](https://webpack.github.io/docs/configuration.html).
