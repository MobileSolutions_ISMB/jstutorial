# 4. ReactJS

## 4.1. Introduction

In the previous chapter we explored AngularJS, a fully featured framework that allows a developer to build entire applications by following its quite strict structure and paradigms. This chapter will focus on ReactJS, which is **not** a framework. Even though ReactJS and AngularJS are often compared, it is arguably not a correct comparison since all ReactJS does is provide a different way to build the _View_ component of a web application. If you were to use it in an MVC architecture, it would be up to you to provide the _Model_ and the _Controller_ logic, as well as all the other code to glue things together. Of course, you won't be using ReactJS in an MVC architecture, but we'll talk more about that later.

ReactJS has shaken the web community quite a bit, first and foremost because of its non-standard syntax. The first thing you need to do before approaching React is to _forget about everything you know to be "right" in web development_. It will look strange, and you will not immediately appreciate its merits.

## 4.2. The JSX syntax

JSX had many developers shake their heads in disapproval when it first came out. Why? Because the developers of ReactJS decided that instead of diving the code "by technology" they were going to divide it "by concern". JSX syntax thus allows to mix JavaScript with something that looks quite a bit like HTML, but make no mistake: it's **NOT** HTML, but instead nothing more than syntactic sugar. Let's take a look at what JSX looks like:

```javascript
import React from 'react';

class HelloWorld extends React.Component {
    render() {
        return (
            <h1>Hello, World!</h1>
        );
    }
}

export default HelloWorld;
```
This should look mostly familiar: a class that extends another one, with an instance method called render that returns... HTML markup. When this is transpiled to JavaScript, it actually becomes this:

```javascript
import React from 'react';

class HelloWorld extends React.Component {
    render() {
        return (
            React.createElement('h1', {}, 'Hello, World!');
        );
    }
}

export default HelloWorld;
```
... which is perfectly regular JavaScript. We'll later see how, while possible, it becomes incredibly inconvenient to write code in anything other than JSX when creating React components.

## 4.3. Configuring Webpack

Since JSX is not standard, and is a superset of JavaScript, we need to transpile it back to regular ES5. This is achieved using Webpack, with a configuration that is not that much different from the one we used in the AngularJS chapter. Create your `webpack.config.js` file and fill it like this:

```javascript
module.exports = {
    context: __dirname + '/src',
    entry: {
        html: './index.html',
        javascript: './index.js'
    },
    output: {
        path: __dirname + '/dist',
        filename: 'app.js'
    },
    resolve: {
        extensions: ['', '.js', '.jsx', '.json'],
        modulesDirectories: ["node_modules", "src"]
    },
    module: {
        loaders: [
            { test: /index\.html/, loader: "file?name=[name].[ext]" },
			{
				test: /\.jsx?$/,
				exclude: /(node_modules)/,
				loader: 'babel',
                query: {
                    presets: ["es2015", "react"]
                }
			},
            { test: /\.css$/, loader: "style-loader!css-loader!autoprefixer-loader" },
            { test: /\.woff2?(\?v=\d+\.\d+\.\d+)?$/,   loader: "url?limit=10000&mimetype=application/font-woff" },
            { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=application/octet-stream" },
            { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    loader: "file" },
            { test: /\.png(\?v=\d+\.\d+\.\d+)?$/,    loader: "file" },
            { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=image/svg+xml" },       
        ]  
    },
	devtool: 'source-map'
}
```
We added the `jsx` extension to the resolved extensions (so that we can `import` JSX files without specifying their extension every time), and modified the object for the _babel_ loader so that it tests true for `jsx` as well as `js` files; we also added the _react_ preset, which we're going to install:

```bash
npm install --save-dev babel-preset-react
```

Now we're good to go!

## 4.4. Hello, React!

Next, we're going to install the required libraries to write our first ReactJS application: `react` and `react-dom`. The first contains the core of ReactJS, while the other one allows us to have React interact with the DOM tree when running in a browser - that's not in the core because React can also run on the server!

```bash
npm install --save react react-dom
```

As usual, we'll create an `src` folder and within it an `index.jsx`:

```javascript
import React from 'react';
import ReactDOM from 'react-dom';

class HelloWorld extends React.Component {
    render() {
        return (
            <h1>Hello, World!</h1>
        );
    }
}

ReactDOM.render(
    <HelloWorld />,
    document.querySelector('#main')
);
```

We'll also copy the same `index.html` we used in Chapter 2 (ES6).

Let's look at the code: we created a simple `HelloWorld` component that prints a message, and rendered it within the `#main` HTML element using the `ReactDOM.render` method. The component is used, after being defined, as if it was an HTML tag, and can be self-closing if it doesn't need to have any children. Why? Because just like an HTML tag, it can have attributes. Let's put our component into its own `src/Components/HelloWorld.jsx` file and spice things up a bit:

```javascript
import React from 'react';
import { Component } from 'react';

class HelloWorld extends Component {
    render() {
        return (
            <h1>Hello, {this.props.name}!</h1>
        );
    }
}

HelloWorld.propTypes = {
    name: React.PropTypes.string
}

HelloWorld.defaultProps = {
    name: 'World'
}

export default HelloWorld;
```

We used React's _Properties_, or `props`, to print the name we passed to the component. This means that in our `index.jsx` we can you it like this:

```javascript
import React from 'react';
import ReactDOM from 'react-dom';

import HelloWorld from 'Components/HelloWorld';

ReactDOM.render(
    <HelloWorld name="Bruce Wayne" />,
    document.querySelector('#main')
);
```

and we'll greet the Batman! But what do the `propTypes` and `defaultProps` assignment mean? We simply told ReactJS which types the properties will have on the component (by following [this](https://facebook.github.io/react/docs/reusable-components.html#prop-validation) syntax) and what their default value is if they're not specified. Keep in mind that, while both `propTypes` and `defaultProps` are _optional_, their use is **strongly** recommended.

You might have noticed that in our new `HelloWorld` component we interpolated the the `name` property by using the `{this.props.name}` notation. ReactJS allows us to interpolate variables into our "template", and automatically re-renders a component whenever a change on that property occurs. An important thing you should know is that, within a component, properties are _immutable_. This means that properties **cannot** be changed from the code inside a component, but must be changed from the outside. While it may seem like a limitation, this ensures data consistency and avoids situation where data is not in sync.

> It's important to know that, under the hood, ReactJS implements something called a _Virtual DOM_. This is a graph-like representation of the DOM tree that is being rendered, and whenever an update is triggered ReactJS will efficiently change only what needs to be changed - nothing more - and will re-render only that, leaving the rest as it is. This has an extremely positive impact on performance, but makes it so that manually manipulating the DOM in any way becomes impossible when using ReactJS: a manual manipulation would result in a mismatch between the Virtual DOM and the actual DOM, and therefore in a crash.

## 4.5. Stateless and Stateful Components

A component can either be stateless of stateful. Our `HelloWorld` component, for example, can be considered stateless as it doesn't keep track of any data and simply renders itself along with any property it's given. A stateless component can be written with a shorthand syntax, as follows:

```javascript
const HelloWorld = (props) => <h1>Hello, {props.name}</h1>;
```

The clear advantage of this syntax is that the whole "boilerplate" is gone, replaced by a simply arrow function. The clear disadvantage is that we have lost property types and defaults, and we are clearly more limited in what we can do with this component. It's obvious that this syntax should only be used with very simple components.

A stateful component, on the other hand, can be a component that keeps an internal state or simply a component that requires a backing instance in order to implement some logic. Let's consider a basic `Counter.jsx` component:

```javascript
import React from 'react';
import { Component } from 'react';

class Counter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            count: 0
        };
    }

    componentDidMount() {
        this.startCounting();
    }

    startCounting() {
        this.interval = setInterval(() => {
            let count = this.state.count + 1;

            this.setState({
                count
            });
        }, 1000);
    }

    stopCounting() {
        clearInterval(this.interval);
    }

    render() {
        return(
            <div>
                <h1>Awesome Counter</h1>
                <hr />
                <p>We have counted up to {this.state.count}</p>

                <button onClick={this.startCounting.bind(this)}>Start!</button>
                <button onClick={this.stopCounting.bind(this)}>Stop!</button>
            </div>
        );
    }
}

export default Counter;
```

More than one thing is going on here:

- We have added a `constructor`, which receives the starting `props` and calls the `super` method, passing them to it;
- We have set the initial state of the component within the `constructor`. Note that `state` is the instance variable you should use if you want to use ReactJS's built-in state management cycle;
- We have used one of the [Component Lifecycle Methods](https://facebook.github.io/react/docs/component-specs.html#lifecycle-methods), `componentDidMount`, to start the counter;
- We have added two buttons in the `render` method, binding each respective `onClick` event to an instance method. `bind`ing the methods to the current instance is **mandatory**;
- The callback passed to the `setInterval` call uses the `setState` instance method on the component. This causes ReactJS to update the state of the component and triggers a re-render. Note that updating `this.state` manually won't yield the same effect!

## 4.6. The Notice Board Application

As with the previous chapters, we'll touch on some of the other ReactJS features by implementing our now well-known Notice Board application. Let's start by installing our remaining dependencies:

```bash
npm install bootstrap jquery axios --save
```

Following the same structure of the previous chapters, our `index.jsx` will be quite simple and only be used to bootstrap our application:

```javascript
global.jQuery = require('jquery');
require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/js/bootstrap.js');

import React from 'react';
import ReactDOM from 'react-dom';

import Application from 'Application';

ReactDOM.render(
    <Application />,
    document.querySelector('#main')
);
```

> Remember! Update the `entrypoint` key in `webpack.config.js`, replacing `index.js` with `index.jsx`!

Our `Application` component will have the responsibility of keeping track of the messages, as well as to render its child components in order to display the new message form and the message list; it will also have to handle all the requests to the the server, in order to fetch existing messages and post new ones. We could implement it like this:

```javascript
import React from 'react';
import { Component } from 'react';
import http from 'axios';

import NewMessage from 'NewMessage';
import MessageList from 'MessageList';

class Application extends Component {
    constructor() {
        super();

        this.state = {
            messages: []
        }
    }

    componentDidMount() {
        this.fetchMessages();
    }

    fetchMessages() {
        http.get('http://localhost:3000/posts')
            .then(
                response => {
                    this.setState({ messages: response.data.data.reverse() });
                },
                error => {
                    console.log(error);
                }
            )
    }

    postMessage(author, text) {
        http.post('http://localhost:3000/posts', {author, text})
            .then(
                response => {
                    this.fetchMessages();
                },
                error => {
                    console.log(error);
                }
            )
    }

    render() {
        return (
            <div className="container">
                <h1>Notice Board</h1>
                <NewMessage onFormSubmit={this.postMessage.bind(this)}/>
                <hr/>
                <MessageList messages={this.state.messages} />
            </div>
        )
    }
}

export default Application;
```

The code above should look quite familiar. One thing of interest is what happens in the `render` method: the `NewMessage` component is binded to an instance method on the `Application` component through the `onFormSubmit` property; this means that the `NewMessage` component will not handle the `POST` logic itself, but will rather _delegate_ it "upwards" to its parent.

## 4.7. Forms and component refs

Our new message component would look like this:

```javascript
import React from 'react';
import { Component } from 'react';

class NewMessage extends Component {
    handleFormSubmit(event) {
        event.preventDefault();

        this.props.onFormSubmit(this.refs.author.value, this.refs.text.value);
    }

    render() {
        return (
            <form onSubmit={this.handleFormSubmit.bind(this)} >
                <div className="form-group">
                    <label>Author</label>
                    <input type="text" className="form-control" ref="author" />
                </div>
                <div className="form-group">
                    <label>Text</label>
                    <input type="text" className="form-control" ref="text" />
                </div>
                <div className="form-group text-right">
                    <button type="submit" className="btn btn-lg btn-success">Send!</button>
                </div>
            </form>
        );
    }
}

export default NewMessage;
```

Since ReactJS doesn't offer two-way data binding, we need to directly fetch the value of our fields when submitting a form. Fortunately, ReactJS offers `refs` to easily allow us to solve this problem:
- Any component (or HTML tag) can be attributed a _unique_ ref within the parent component;
- We can reference any `ref` within the parent component through its `this.refs` internal dictionary. The value returned by calling a ref is the DOM element itself, so we can query any of its properties (such as the `value` for an `input` tag);

Our `NewMessage` component does this when calling the method binded to its `onFormSubmit` property, as soon as the form it renders is submitted.

## 4.8. Lists!

In order to show all the messages, we need to display a list. We'll create a `MessageList` component:

```javascript
import React from 'react';

import Message from 'Message';

const MessageList = (props) => (
    <div>
        {props.messages.map(
            message => (
                <Message
                    text={message.text}
                    author={message.author}
                    date={message.date}
                    key={message.date}
                />
            )
        )}
    </div>
);

export default MessageList;
```

As we said before, whenever a component does not need to keep track of an internal state and does not need to reference anything within itself, we can use the stateless syntax to avoid writing boilerplate code. In this case, our `MessageList` simply renders as much `Message` components as there are messages in the `props.messages` array. A couple of things of interest:
- One of the most efficient ways to create lists starting from array is to use an higher order function such as `map`, which is called on any array and returns each value "transformed" as specified in the callback;
- Each element of a list, in ReactJS, **must** have a `key` property to allow ReactJS to properly identify it;

What does the `Message` component look like? Like this:

```javascript
import React from 'react';

let Message = (props) => (
    <div className="panel panel-default">
        <div className="panel-body">
            {props.text}
        </div>
        <div className="panel-footer">
            Written by <strong>{props.author}</strong> on <em>{props.date}</em>
        </div>
    </div>
);

export default Message;
```

Yet again, a simple stateless component which simply renders the properties it's passed.

And that's about all there is to our Notice Board application. You can try running it, and you'll see that it will do its thing quite smoothly.
