module.exports = {
    context: __dirname + '/src',
    entry: {
        html: './index.html',
        javascript: './index.jsx'
    },
    output: {
        path: __dirname + '/dist',
        filename: 'app.js'
    },
    resolve: {
        extensions: ['', '.js', '.jsx', '.json'],
        modulesDirectories: ["node_modules", "src"]
    },
    module: {
        loaders: [
            { test: /index\.html/, loader: "file?name=[name].[ext]" },
			{
				test: /\.jsx?$/,
				exclude: /(node_modules)/,
				loader: 'babel',
                query: {
                    presets: ['es2015', 'react']
                }
			},
            { test: /\.css$/, loader: "style-loader!css-loader!autoprefixer-loader" },
            { test: /\.woff2?(\?v=\d+\.\d+\.\d+)?$/,   loader: "url?limit=10000&mimetype=application/font-woff" },
            { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=application/octet-stream" },
            { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,    loader: "file" },
            { test: /\.png(\?v=\d+\.\d+\.\d+)?$/,    loader: "file" },
            { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,    loader: "url?limit=10000&mimetype=image/svg+xml" },
        ]
    },
	devtool: 'source-map'
}
