import React from 'react';

import Message from 'Message';

const MessageList = (props) => (
    <div>
        {props.messages.map(
            message => (
                <Message
                    text={message.text}
                    author={message.author}
                    date={message.date}
                    key={message.date}
                />
            )
        )}
    </div>
);

export default MessageList;
