global.jQuery = require('jquery');
require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/js/bootstrap.js');

import React from 'react';
import ReactDOM from 'react-dom';

import Application from 'Application';

ReactDOM.render(
    <Application />,
    document.querySelector('#main')
);
