import React from 'react';
import { Component } from 'react';
import http from 'axios';

import NewMessage from 'NewMessage';
import MessageList from 'MessageList';

class Application extends Component {
    constructor() {
        super();

        this.state = {
            messages: []
        }
    }

    componentDidMount() {
        this.fetchMessages();
    }

    fetchMessages() {
        http.get('http://localhost:3000/posts')
            .then(
                response => {
                    this.setState({ messages: response.data.data.reverse() });
                },
                error => {
                    console.log(error);
                }
            )
    }

    postMessage(author, text) {
        http.post('http://localhost:3000/posts', {author, text})
            .then(
                response => {
                    this.fetchMessages();
                },
                error => {
                    console.log(error);
                }
            )
    }

    render() {
        return (
            <div className="container">
                <h1>Notice Board</h1>
                <NewMessage onFormSubmit={this.postMessage.bind(this)}/>
                <hr/>
                <MessageList messages={this.state.messages} />
            </div>
        )
    }
}

export default Application;
