global.jQuery = require('jquery');
require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/js/bootstrap.js');

import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import MessagesReducer from 'messages/Reducer';

import Application from 'core/Application';

let reducers = combineReducers({
    messages: MessagesReducer
});

const finalCreateStore = compose(
	applyMiddleware(thunk)
)(createStore);

let store = finalCreateStore(reducers);

ReactDOM.render(
    <Provider store={store}>
        <Application />
    </Provider>,
    document.querySelector('#main')
);
