import DefaultState from './DefaultState';
import {
    MESSAGES_FETCHED,
    MESSAGES_FETCHED_ERROR
} from './Actions';

export default function messages(state = DefaultState, action) {
    switch(action.type) {
        case MESSAGES_FETCHED: {
            return Object.assign({}, state, {
                list: action.messages
            });
        }

        case MESSAGES_FETCHED_ERROR: {
            // We should set have another property in our state to handle errors, I'll leave it to you as an exercise.
            return state;
        } break;

        default: {
            return state;
        } break;
    }
}
