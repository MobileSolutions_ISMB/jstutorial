import http from 'axios';

export const MESSAGES_FETCHED = 'MESSAGES_FETCHED';
export const MESSAGES_FETCHED_ERROR = 'MESSAGES_FETCHED_ERROR';
export function fetchMessages() {
    return dispatch => {
        http.get('http://localhost:3000/posts')
            .then(
                response => {
                    dispatch({
                        type: MESSAGES_FETCHED,
                        messages: response.data.data.reverse()
                    });
                },
                error => {
                    dispatch({
                        type: MESSAGES_FETCHED_ERROR,
                        error
                    });
                }
            )
    }
}

export const MESSAGE_POSTED = 'MESSAGE_POSTED';
export const MESSAGE_POST_ERROR = 'MESSAGE_POST_ERROR';
export function postMessage(author, text) {
    return dispatch => {
        http.post('http://localhost:3000/posts', {author, text})
            .then(
                response => {
                    dispatch({
                        type: MESSAGE_POSTED
                    });

                    dispatch(fetchMessages());
                },
                error => {
                    dispatch({
                        type: MESSAGE_POST_ERROR,
                        error
                    });
                }
            );
    }
}
