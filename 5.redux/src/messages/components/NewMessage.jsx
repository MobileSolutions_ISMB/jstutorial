import React from 'react';
import { Component } from 'react';

class NewMessage extends Component {
    handleFormSubmit(event) {
        event.preventDefault();

        this.props.onFormSubmit(this.refs.author.value, this.refs.text.value);
    }

    render() {
        return (
            <form onSubmit={this.handleFormSubmit.bind(this)} >
                <div className="form-group">
                    <label>Author</label>
                    <input type="text" className="form-control" ref="author" />
                </div>
                <div className="form-group">
                    <label>Text</label>
                    <input type="text" className="form-control" ref="text" />
                </div>
                <div className="form-group text-right">
                    <button type="submit" className="btn btn-lg btn-success">Send!</button>
                </div>
            </form>
        );
    }
}

export default NewMessage;
