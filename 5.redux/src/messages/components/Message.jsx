import React from 'react';

let Message = (props) => (
    <div className="panel panel-default">
        <div className="panel-body">
            {props.text}
        </div>
        <div className="panel-footer">
            Written by <strong>{props.author}</strong> on <em>{props.date}</em>
        </div>
    </div>
);

export default Message;
