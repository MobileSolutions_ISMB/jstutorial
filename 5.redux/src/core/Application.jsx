import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';

import NewMessage from 'messages/components/NewMessage';
import MessageList from 'messages/components/MessageList';

import * as MessagesActions from 'messages/Actions';

class Application extends Component {
    constructor() {
        super();
    }

    componentDidMount() {
        this.props.dispatch(MessagesActions.fetchMessages());
    }

    postNewMessage(author, text) {
        let { dispatch } = this.props;

        dispatch(MessagesActions.postMessage(author, text));
    }

    render() {
        let { messages } = this.props;

        return (
            <div className="container">
                <h1>Notice Board</h1>
                <NewMessage onFormSubmit={this.postNewMessage.bind(this)} />
                <hr/>
                <MessageList messages={messages.list} />
            </div>
        )
    }
}

let select = state => state;

export default connect(select)(Application);
