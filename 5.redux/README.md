# 5. Redux

## 5.1. Introduction

We have seen how ReactJS allows us to easily write components, and how these components can, if required, handle some application logic. One thing we should not forget, though, is what a ReactJS component is supposed to be: a **UI** element, not something that handles business logic. When considering an hypothetical MVC architecture, we stated that ReactJS could be used to fill the role of the _View_, and yet we wrote an entire application using React and a couple of other libraries. This approach can be used for rapid prototyping, and maybe even small applications, but leaves much to be desired when writing 'real-world' applications meant to serve a reasonable number of users and/or handle a certain level of complexity.

While ReactJS could indeed be used in an MVC stack, the current consensus is to use it in conjunction with a data architecture called [_Flux_](https://facebook.github.io/flux/). What's important here is that Flux is neither a library nor a framework, but rather a paradigm - a general indication on how you should structure your application. The main idea behind Flux is that it's all about _data_, and data flows in one and only one direction. Every time an _action_ is triggered, a _dispatcher_ takes care of forwarding this action to a _store_, which contains a part of the the current application's _state_. An action causes the state of the application to change, and the _view_ to be updated. Where do actions come from? They can come either from a view, following a user interaction, or from somewhere else - as a result of an asynchronous operation, for example. Another important aspect of the Flux paradigm is that the state of an application can be, at any moment, completely represented through an arbitrarily complex object. To recap, then: an action is sent by the dispatcher to a store, which uses it to alter the state of an application, causing the view to update.

Just like many MVC frameworks and libraries have come to life through the years, many Flux frameworks and libraries have begun sprouting as of late. The one we'll be taking a look at in this chapter is the one getting the most traction: [_Redux_](http://redux.js.org/).

## 5.2. The three principles of Redux

Redux applies its own spin to the Flux paradigm, implementing what is definitely not the "vanilla" version but rather a customized (and, arguably, simplified) version of it. Three principles stand behind Redux's architecture:

#### 1. There is a single source of truth

It's common for Flex frameworks to use more than one store - usually one per entity or one per domain. Redux argues against this approach, and proposes that the state is to be kept in a _single_ store, where the entire state of an application is saved. This allows for a simpler structure, and keeps all the information in a single place: a single source of truth.

#### 2. Immutability of the state

Never, for any reason, shall you modify your application state. Once set, a state is written in stone. The state changes only when an action is dispatched, when a **new** version of the state is created merging the old data with the new. This allows to easily track errors, save states and roll back to a previous state.

#### 3. Purity

Whenever an action is dispatched, it's used to create a new state through a **pure** function. A pure function always returns the same output given a specific set of inputs, and never has any kind of side effect. Pure functions that create new versions of the application state are called _reducers_.

## 5.3. State

Since we're going to have a single source of truth, a good place to start when building a _redux_ application is decide how the state is going to look like. If we go back to our Counter application, the state could look like something like this:

```javascript
{
    counter: {
        is_counting: true,
        count: 0
    }
}
```

This can of course be arbitrarily complex, but you should always strive to keep each domain of your state _flat_. If you're going to have nested objects, try using references instead of nesting them into one another - it might be more difficult to think about your structure as flat, but it will be of great help later on, especially with large applications!

## 5.4. Actions

After deciding how the state should look like, we should start thinking about which actions are going to be dispatched in order to alter the state. We could, for example, decide to have three actions: `COUNT_UP`, `COUNT_START` and `COUNT_STOP`. Alongside our actions we're also going to define _Action Creators_, which are **pure** functions that can return either an action or a Promise. For example, an `Actions.js` file could look like this:

```javascript
export const COUNT_UP = 'COUNT_UP';
export function countUp() {
    return {
        type: COUNT_UP
    }
}

export const COUNT_START = 'COUNT_START';
export function countStart() {
    return {
        type: COUNT_START
    }
}


export const COUNT_STOP = 'COUNT_STOP';
export function countStop() {
    return {
        type: COUNT_STOP
    }
}
```

## 5.5. Reducers

Whenever an action is dispatched, a _Reducer_ can react to that action and change the state (or _reduce_ the state). Usually, a reducer will take in the current state of the application and the action that is being dispatched, and return a new application state. While Redux keeps all the data in a single store, a Reducer can handle just a limited domain of the state so that you don't have to keep all your code together in a single file. Reducers can, of course, be combined in order to allow the manipulation of the entire application state.

The `Reducer.js` file for our counter could be like this:

```javascript
import { COUNT_UP, COUNT_START, COUNT_STOP } from 'Actions';
import DefaultState from 'DefaultState';

export default function counter(state = DefaultState, action) {
    switch(action.type) {
        case COUNT_UP: {
            let count = state.count + 1;
            return Object.assign({}, state, {
                count
            });
        } break;

        case COUNT_START: {
            return Object.assign({}, state, {
                is_counting: true
            });
        } break;

        case COUNT_STOP: {
            return Object.assign({}, state, {
                is_counting: false
            });
        } break;

        default: break;
    }

    return state;
}
```
As you can see, we have imported a default state, which is going to look exactly how you would expect the "counter" domain state to look like when the application starts up:

```javascript
const state = {
    is_counting: false,
    count: 0
};

export default state;
```

Our reducer does nothing more than switching the `type` property of the action, and return a new, mutated state accordingly.

## 5.6. Notice Board Application

Let's leave behind our trivial Counter example and implement our Notice Board Application yet again, this time using _redux_ as our framework and ReactJS as our _View_ component. Let's start from our ReactJS Notice Board and add some dependencies:

```bash
npm install --save redux react-redux redux-thunk
```

While we're going to keep most of our code, let's change the directory structure and move things around a little bit:

```bash
+ src/
    + core/
        - Application.jsx
    + messages/
        + components/
            - NewMessage.jsx
            - Message.jsx
            - MessageList.jsx
    - index.js
    - index.html
```

We created a `core` folder, where we'll keep those files that represent the "inner core" of our application, and a `messages` folder that is used to keep all things related to the "messages" feature of our application (the only feature, in this case). All the components related to the "messages" feature go into the `components` subfolder of `messages`.

> **Remember!** When moving files around, always check how you reference them across your application and fix the paths if they are no longer correct! You'll need to do this here, if you want things to work!

Like we did before, we're going to think about how our application state is going to look like:

```javascript
{
    messages: {
        list: []
    },
}
```

That's... not very complex, but neither is our application. Let's create a new `DefaultState.js` file in our `messages` folder:

```javascript
const state = {
    list: []
};

return default state;
```

Our application is quite simple, but we're still going to keep its state structure divided in 'domains': this will help adding features to it in the future!

## 5.7. Asynchronous actions

Since we've defined a base state, we can decide which actions are going to cause our state to change. Our application has two, and they are both asynchronous: a new message is created, all messages are fetched from the server. _Redux_ actions are, by definition, synchronous and reducers are pure functions, so how are we going to approach the issue of handling an asynchronous request? There are multiple ways, but we're going to use _Thunks_. Thunks are pure functions that instead of returning an action, return another function capable of dispatching actions.

Let's create an `Actions.js` file inside our `messages` folder and add the action to fetch all messages from the server:

```javascript
import http from 'axios';

export const MESSAGES_FETCHED = 'MESSAGES_FETCHED';
export const MESSAGES_FETCHED_ERROR = 'MESSAGES_FETCHED_ERROR';
export function fetchMessages() {
    return dispatch => {
        http.get('http://localhost:3000/posts')
            .then(
                response => {
                    dispatch({
                        type: MESSAGES_FETCHED,
                        messages: response.data.data.reverse()
                    });
                },
                error => {
                    dispatch({
                        type: MESSAGES_FETCHED_ERROR,
                        error
                    });
                }
            )
    }
}
```

What happens here is that when the `fetchMessages` action is dispatched, _Redux_ automatically knows, thanks to `redux-thunk`, to  call the function returned by the action. This allows us to write actions that are still pure functions (there are no side effects) while still being able to deal with promises and asynchronous operations. Once the promise is resolved or rejected, other actions can be dispatched to update the state in response to the asynchronous action.

Now we just need to create a `messages/Reducer.js` file:

```javascript
import DefaultState from './DefaultState';
import {
    MESSAGES_FETCHED,
    MESSAGES_FETCHED_ERROR
} from './Actions';

export default function messages(state = DefaultState, action) {
    switch(action.type) {
        case MESSAGES_FETCHED: {
            return Object.assign({}, state, {
                list: action.messages
            });
        }

        case MESSAGES_FETCHED_ERROR: {
            // We should set have another property in our state to handle errors, I'll leave it to you as an exercise.
            return state;
        } break;

        default: {
            return state;
        } break;
    }
}
```

## 5.8. Connecting React to Redux

Since the base logic of our application is mostly written, we now have to make the UI 'talk' with the business logic. We'll have to follow these steps:

- Combine all our reducers into a single one (even if we have just one);
- Create a store to save the state into;
- Connect our views to the state, in order to be able to read it and dispatch actions;

Let's go back to our `index.jsx`:

```javascript
global.jQuery = require('jquery');
require('bootstrap/dist/css/bootstrap.css');
require('bootstrap/dist/js/bootstrap.js');

import React from 'react';
import ReactDOM from 'react-dom';

// -- Redux Stuff
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import MessagesReducer from 'messages/Reducer';

let reducers = combineReducers({
    messages: MessagesReducer
});

const finalCreateStore = compose(
	applyMiddleware(thunk)
)(createStore);

let store = finalCreateStore(reducers);
// --

import Application from 'core/Application';

ReactDOM.render(
    <Provider store={store}>
        <Application />
    </Provider>,
    document.querySelector('#main')
);
```

That's quite a lot of code! What it actually does is:

- It `import`s the Reducer and, by using _Redux_'s `combineReducers` function, creates a `reducers` function that will be used to create the store;

- It defines a `finalCreateStore` function by _composing_ the default `createStore` function with some middlewares (in our case, `redux-thunk`);

- It creates a `store` using the `finalCreateStore` function, by passing it the `reducers` function as an argument. The `store` will contain our whole application state, and is the object on which the `dispatch` method will be called;

- Using the `Provider` component from `react-redux` we pass the `store` down in our component hierarchy, allowing us to _connect_ our components with our `store`;

We still need to modify our `core/Application.jsx` file:

```javascript
import React from 'react';
import { Component } from 'react';
import { connect } from 'react-redux';

import NewMessage from 'messages/components/NewMessage';
import MessageList from 'messages/components/MessageList';

class Application extends Component {
    constructor() {
        super();
    }

    render() {
        let { messages } = this.props;

        return (
            <div className="container">
                <h1>Notice Board</h1>
                <hr/>
                <MessageList messages={messages.list} />
            </div>
        )
    }
}

let select = state => state;

export default connect(select)(Application);
```

Whenever we want to _connect_ a component to the application state, we just need to define a `select` function that receives the entire state as an argument and returns the parts of the state we want to be accessible to the component; we then `export` the component by wrapping it in a call to the function returned by `connect(select)`.

Since our Application is connected to the entire state, we can access it through the component's properties. For example, the entire list of messages is accessible through the `messages.list` property on our `Application` component.

## 5.9. Dispatching Actions

When connecting a component to the _Redux_ Application state, we also get a reference to the `dispatch` method in our component's properties. The `dispatch` method is how actions are, as the name itself suggests, dispatched. Once an action is dispatched, it gets intercepted by the reducers and those reducers that are equipped to handle it will then alter the application's state accordingly.

This means that we can call any action we have defined by passing it to this method. If we want, for example, our application to fetch all the messages from the server as soon as the `Application` component mounts, we'll modify `Application.jsx` like so:

```javascript
//...
import * as MessagesActions from 'messages/Actions';

class Application extends Component {
    //...
    componentDidMount() {
        let { dispatch } = this.props;

        dispatch(MessageActions.fetchMessages());
    }
    //...
}
//...
```

Since the `fetchMessages` action is asynchronous, it will actually trigger an HTTP request towards our server, and only after the server has responded will the state change. Once the status is updated, the props passed to our `Application` component will be updated, and ReactJS will take care of refreshing the view.

## 5.10. Posting new messages

The process of posting new messages is not that different from the one we used above to fetch them. The first we're going to do is to define the action in `messages/Actions.js`:

```javascript
//...
export const MESSAGE_POSTED = 'MESSAGE_POSTED';
export const MESSAGE_POST_ERROR = 'MESSAGE_POST_ERROR';
export function postMessage(author, text) {
    return dispatch => {
        http.post('http://localhost:3000/posts', {author, text})
            .then(
                response => {
                    dispatch({
                        type: MESSAGE_POSTED
                    });

                    dispatch(fetchMessages());
                },
                error => {
                    dispatch({
                        type: MESSAGE_POST_ERROR,
                        error
                    });
                }
            );
    }
}
```

The only difference is that when the promise resolves successfully it dispatches two actions instead of one: the first is the action that signals the success of the post operations (which will actually do nothing), the second is the `fetchMessages` action, which will fetch the messages from the server. The optimal solution would be to actually add the new message (which is returned as the payload of the `POST` call) to the existing array of messages, but we're not here to optimize :)

The last step to successfully post messages is to properly dispatch the action, and to do so we'll put our `NewMessage` form back into our `Application.jsx`:

```javascript
//...
class Application extends Component {
    constructor() {
        super();
    }

    componentDidMount() {
        let { dispatch } = this.props;

        dispatch(MessageActions.fetchMessages());
    }

    postNewMessage(author, text) {
        let { dispatch } = this.props;

        dispatch(MessagesActions.postMessage(author, text));
    }

    render() {
        let { messages } = this.props;

        return (
            <div className="container">
                <h1>Notice Board</h1>
                <NewMessage onFormSubmit={this.postNewMessage.bind(this)} />
                <hr/>
                <MessageList messages={messages.list} />
            </div>
        )
    }
}
//...
```

And that's about it, you now have a very simple _Redux_ application running!
